//
//  CoreDataInit.swift
//  benzo
//
//  Created by Michael Artuerhof on 28.02.18.
//  Copyright © 2018 MLN Group. All rights reserved.
//

import UIKit
import MagicalRecord

class CoreDataInit: UIResponder {
    func description() -> String {
        return "CoreDataInit"
    }

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        var bundles = [Bundle.main]
        if let b = Bundle(identifier: "com.bearmonti.bcache-swift") {
            bundles.append(b)
        }
        if let cpod = Bundle(identifier: "org.cocoapods.bcache-swift") {
            bundles.append(cpod)
        }
        MagicalRecord.setShouldAutoCreateManagedObjectModel(false)
        NSManagedObjectModel.mr_setDefaultManagedObjectModel(NSManagedObjectModel.mergedModel(from: bundles))
        MagicalRecord.setupCoreDataStack(withAutoMigratingSqliteStoreNamed: "benzo")
        return true
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        NSManagedObjectContext.mr_default().mr_saveToPersistentStore(completion: { (success, err) in
            if let e = err {
                print("error while saving core data: \(e.localizedDescription)")
            }
        })
    }
}
