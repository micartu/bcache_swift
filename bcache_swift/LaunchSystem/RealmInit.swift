//
//  RealmInit.swift
//  benzo
//
//  Created by Michael Artuerhof on 30.05.19.
//  Copyright © 2019 MLN Group. All rights reserved.
//

import Foundation
import RealmSwift

class RealmInit: UIResponder {
    func description() -> String {
        return "RealmInit"
    }

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        let defaultURL = Realm.Configuration.defaultConfiguration.fileURL!
        print("realm default file url: '\(defaultURL)'")
        let folderPath = defaultURL.deletingLastPathComponent().path
        let fm = FileManager.default
        // make sure we can use realm db from background-mode of the app:
        try! fm.setAttributes([FileAttributeKey.protectionKey: FileProtectionType.none],
                              ofItemAtPath: folderPath)
        // ensure that the documents folder is created (realm would store its data here):
        if let documentsPath = fm.urls(for: .documentDirectory,
                                       in: .userDomainMask).first {
            do {
                try fm.createDirectory(at: documentsPath,
                                       withIntermediateDirectories: true,
                                       attributes: nil)
            }
            catch {
                print("Cannot create directory at path name: \(documentsPath)")
            }
        }
        let migrationBlock: MigrationBlock = { migration, oldSchemaVersion in
            if oldSchemaVersion < 1 {
            }
            print("Realm: Migration complete.")
        }
        Realm.Configuration.defaultConfiguration =
            Realm.Configuration(schemaVersion: 0, migrationBlock: migrationBlock)
        return true
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
    }
}
