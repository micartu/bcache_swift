//
//  SecretService.swift
//  benzo
//
//  Created by Michael Artuerhof on 26.04.18.
//  Copyright © 2018 MLN Group. All rights reserved.
//

import Foundation

protocol SecretService {
    var authorized: Bool { get }
    var isEncrypted: Bool { get set }
    var isAuthCreated: Bool { get set }
    var pwd: String { get set }
    var magic: String { get set }
    var session: String { get set }
    var user: String { get set }
    var password: String { get set }
    var cardId: String { get set }
    var uid: String { get set }
    var isSocialAuth: Bool { get set }
    var socialAuthType: String { get set }
    var socialAuthToken: String { get set }
    var socialAuthTokenId: String { get set }
    var socialAuthExpire: Date? { get set }

    func encrypt(_ s: String) -> String
    func decrypt(_ s: String) -> String
    func canAccessKeychain() -> Bool
    func restoreKeyFromEnclave() -> Bool
    func saveKeyToEnclave()
    func erase()
}
