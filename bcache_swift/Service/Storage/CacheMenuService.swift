//
//  CacheMenuService.swift
//  benzo
//
//  Created by Michael Artuerhof on 03.06.19.
//  Copyright © 2019 MLN Group. All rights reserved.
//

import Foundation

protocol CacheMenuService {
    func getStoredMenuSectionsForCurrentUser() -> [MenuSection]?
    func storedMenuSectionsForCurrentUserExpired() -> Bool
    func storeForCurrentUser(menuSections ms: [MenuSection],
                             completion: @escaping (() -> Void))
}
