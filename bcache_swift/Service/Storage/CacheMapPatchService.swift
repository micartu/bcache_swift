//
//  CacheMapPatchService.swift
//  benzo
//
//  Created by Michael Artuerhof on 30.05.19.
//  Copyright © 2019 MLN Group. All rights reserved.
//

import Foundation

protocol CacheMapPatchService {
    func exists(patch id: Int) -> Bool
    func isExpired(patch id: Int,
                   expireInterval: TimeInterval,
                   on date: Date) -> Bool
    func get(patch id: Int) -> MapPatchBase?
    func getStationsIn(patch id: Int) -> [Station]
    func create(patch id: Int, completion: @escaping (() -> Void))
    func delete(patch id: Int, completion: @escaping (() -> Void))
}
