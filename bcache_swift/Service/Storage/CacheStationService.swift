//
//  CacheStationService.swift
//  benzo
//
//  Created by Michael Artuerhof on 29.05.19.
//  Copyright © 2019 MLN Group. All rights reserved.
//

import Foundation

protocol CacheStationService {
    func get(station id: Int) -> Station?
    func getStationsIn(lon1: Double,
                       lat1: Double,
                       lon2: Double,
                       lat2: Double) -> [Station]
    func create(station: Station, completion: @escaping ((Int) -> Void))
    func create(stations sts: [Station], completion: @escaping (([Int]) -> Void))
    func update(station: Station, completion: @escaping (() -> Void))
    func delete(station s: Station, completion: @escaping (() -> Void))
    func delete(stations sts: [Station], completion: @escaping (() -> Void))
}
