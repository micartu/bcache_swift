//
//  Storage+Station.swift
//  benzo
//
//  Created by Michael Artuerhof on 05.06.19.
//  Copyright © 2019 MLN Group. All rights reserved.
//

import Foundation
import MagicalRecord

extension Storage: CacheStationService {
    func get(station id: Int) -> Station? {
        if let s = get(station: id, in: getContext()) {
            return convert(station: s)
        }
        return nil
    }

    func getStationsIn(lon1: Double, lat1: Double, lon2: Double, lat2: Double) -> [Station] {
        return getStationsIn(lon1: lon1, lat1: lat1,
                             lon2: lon2, lat2: lat2, in: getContext())
    }

    func create(station: Station, completion: @escaping ((Int) -> Void)) {
        var id = 0
        MagicalRecord.save({ [unowned self] context in
            let s = self.create(station: station, in: context)
            let p = self.getOrCreate(patch: station.patchId, in: context)
            s.patch = p
            id = Int(s.id)
            self.saveIds()
        }, completion: { (success, err) in
                completion(id)
        })
    }

    func create(stations st: [Station], completion: @escaping (([Int]) -> Void)) {
        var ids = [Int]()
        MagicalRecord.save({ [unowned self] context in
            var patches = [CMapPatch]()
            var lastPatch: CMapPatch? = nil
            for s in st {
                let cs = self.create(station: s, in: context)
                var found = false
                ids.append(Int(cs.id))
                if let lp = lastPatch {
                    if lp.id == s.patchId {
                        cs.patch = lp
                        found = true
                    }
                }
                if !found {
                    let p = self.get(patch: s.patchId, from: &patches, in: context)
                    cs.patch = p
                    lastPatch = p
                }
            }
            self.saveIds()
            }, completion: { (success, err) in
                completion(ids)
        })
    }

    func update(station: Station, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            if let u = self.get(station: station.id, in: context) {
                self.update(station: u, from: station)
            } else {
                _ = self.create(station: station, in: context)
            }
        }, completion: { success, err in
                completion()
        })
    }

    func delete(station s: Station, completion: @escaping (() -> Void)) {
        delete(stations: [s], completion: completion)
    }

    func delete(stations sts: [Station], completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            for s in sts {
                self.delete(station: s, in: context)
            }
        }, completion: { (success, err) in
                completion()
        })
    }

    // MARK: - Private/Internal

    // MARK: - getters

    private func getStationsIn(lon1: Double,
                       lat1: Double,
                       lon2: Double,
                       lat2: Double,
                       in context: NSManagedObjectContext) -> [Station] {
        let predicate = NSPredicate(format: "(lon >= %g) AND (lon <= %g) AND (lat >= %g) AND (lat <= %g)",
                                    lon1, lon2, lat2, lat1)
        return getStations(with: predicate, in: context)
    }

    internal func getStationsIn(patch id: Int, in context: NSManagedObjectContext) -> [Station] {
        let predicate = NSPredicate(format: "patch.id == %d", id)
        return getStations(with: predicate, in: context)
    }

    private func getStations(with predicate: NSPredicate,
                             in context: NSManagedObjectContext) -> [Station] {
        var out = [Station]()
        if let stations = CStation.mr_findAll(with: predicate,
                                              in: context) as? [CStation] {
            for s in stations {
                let station = convert(station: s)
                out.append(station)
            }
        }
        return out
    }

    private func get(station id: Int, in context: NSManagedObjectContext) -> CStation? {
        let predicate = NSPredicate(format: "id == %d", id)
        return CStation.mr_findFirst(with: predicate, in: context)
    }

    // MARK: - create

    private func create(station: Station, in context: NSManagedObjectContext) -> CStation {
        let s = CStation.mr_createEntity(in: context)!
        lock.lock()
        s.id = stationId
        stationId += 1
        lock.unlock()
        update(station: s, from: station)
        return s
    }

    // MARK: - convertion

    internal func convert(station s: CStation) -> Station {
        let pid: Int
        #if false // test if crash will go away
        if let p = s.patch {
            pid = Int(p.id)
        } else {
            pid = getPatchId(lon: s.lon, lat: s.lat)
        }
        #else
        pid = getPatchId(lon: s.lon, lat: s.lat)
        #endif
        return Station(id: Int(s.id),
                       patchId: pid,
                       name: safeString(s.name),
                       descr: safeString(s.descr),
                       address: safeString(s.address),
                       inCarBuyFuel: s.inCarBuyFuel,
                       prices: "", // TODO: replace me
            imgSmall: safeString(s.imgSmall),
            imgBig: safeString(s.imgBig),
            lon: s.lon,
            lat: s.lat,
            countCollumn: Int(s.columnCount))
    }

    private func update(station cs: CStation, from s: Station) {
        cs.columnCount = Int16(s.countCollumn)
        cs.descr = s.descr
        cs.lon = s.lon
        cs.lat = s.lat
        cs.name = s.name
        cs.inCarBuyFuel = s.inCarBuyFuel
        cs.address = s.address
        cs.imgSmall = s.imgSmall
        cs.imgBig = s.imgBig
    }
}
