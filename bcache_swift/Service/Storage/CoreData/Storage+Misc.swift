//
//  Storage+Misc.swift
//  benzo
//
//  Created by Michael Artuerhof on 06.06.19.
//  Copyright © 2019 MLN Group. All rights reserved.
//

import Foundation
import MagicalRecord

extension Storage: CacheMiscService {
    func authenticated() -> Bool {
        let user = secrets.user
        let pass = secrets.password
        let session = secrets.session
        if ((user.count > 0 && pass.count > 0) || secrets.isSocialAuth) &&
            session.count > 0 {
            return true
        }
        return false
    }

    func onBoarded() -> Bool {
        let defaults = UserDefaults.standard
        let ret = defaults.bool(forKey: Const.keys.shownOnBoarding)
        if !ret {
            defaults.set(true, forKey: Const.keys.shownOnBoarding)
            defaults.synchronize()
        }
        return ret
    }
}
