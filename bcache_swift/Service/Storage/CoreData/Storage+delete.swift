//
//  Storage+delete.swift
//  benzo
//
//  Created by Michael Artuerhof on 27.04.18.
//  Copyright © 2018 MLN Group. All rights reserved.
//

import Foundation
import MagicalRecord

extension Storage: CacheClearanceService {
    func deleteAllCache(completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            self.deleteAllMapPatches(in: context)
            if let u = self.getUser(self.secrets.user, in: context) {
                self.deleteAllLocPoints(for: u, in: context)
                self.deleteAllTransactions(for: u, in: context)
                self.deleteAllMessages(for: u, in: context)
                self.deleteAllPromos(for: u, in: context)
                self.deleteAllMenuSections(for: u, in: context)
            }
        }, completion: { (success, err) in
                completion()
        })
    }

    func deleteAllMapPatches(completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [weak self] context in
            self?.deleteAllMapPatches(in: context)
        }, completion: { (success, err) in
                completion()
        })
    }

    func deleteAllTransaction(completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            if let u = self.getUser(self.secrets.user, in: context) {
                self.deleteAllTransactions(for: u, in: context)
            }
        }, completion: { (success, err) in
                completion()
        })
    }

    func deleteAllWebPages(completion: @escaping (() -> Void)) {
        MagicalRecord.save({ context in
            if let pages = CWebPage.mr_findAll(in: context) as? [CWebPage] {
                for p in pages {
                    p.mr_deleteEntity(in: context)
                }
            }
        }, completion: { (success, err) in
                completion()
        })
    }

    private func deleteAllMapPatches(in context: NSManagedObjectContext) {
        if let patches = CMapPatch.mr_findAll(in: context) as? [CMapPatch] {
            for p in patches {
                if let stations = p.stations as? Set<CStation> {
                    p.removeFromStations(stations as NSSet)
                    for s in stations {
                        s.mr_deleteEntity(in: context)
                    }
                }
                p.mr_deleteEntity(in: context)
            }
            lock.lock()
            stationId = 1
            lock.unlock()
        }
    }

    internal func deleteAllTransactions(for u: CUser, in context: NSManagedObjectContext) {
        if let tt = u.transactions as? Set<CTransaction> {
            u.removeFromTransactions(tt as NSSet)
            for t in tt {
                t.mr_deleteEntity(in: context)
            }
        }
    }

    internal func deleteAllMessages(for u: CUser, in context: NSManagedObjectContext) {
        if let mes = u.messages as? Set<CMessage> {
            u.removeFromMessages(mes as NSSet)
            for m in mes {
                m.mr_deleteEntity(in: context)
            }
        }
    }

    internal func deleteAllPromos(for u: CUser, in context: NSManagedObjectContext) {
        if let prm = u.promos as? Set<CPromo> {
            u.removeFromPromos(prm as NSSet)
            for p in prm {
                p.mr_deleteEntity(in: context)
            }
        }
    }

    internal func delete(station s: Station, in context: NSManagedObjectContext) {
        let predicate = NSPredicate(format: "id == %d", s.id)
        if let cs = CStation.mr_findFirst(with: predicate, in: context) {
            if let p = cs.patch {
                p.removeFromStations(cs)
                if p.stations?.count == 0 {
                    p.mr_deleteEntity(in: context)
                }
            }
            cs.mr_deleteEntity(in: context)
        }
    }

    internal func delete(webpage p: WebPage, in context: NSManagedObjectContext) {
        let predicate = NSPredicate(format: "id == %d", p.id)
        if let wp = CWebPage.mr_findFirst(with: predicate, in: context) {
            wp.mr_deleteEntity(in: context)
        }
    }

    internal func delete(message m: Message, in context: NSManagedObjectContext) {
        let predicate = NSPredicate(format: "id == %@", m.id)
        if let wm = CMessage.mr_findFirst(with: predicate, in: context) {
            wm.mr_deleteEntity(in: context)
        }
    }
}
