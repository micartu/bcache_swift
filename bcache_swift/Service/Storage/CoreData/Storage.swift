//
//  Storage.swift
//  benzo
//
//  Created by Michael Artuerhof on 23.04.18.
//  Copyright © 2018 MLN Group. All rights reserved.
//

import Foundation
import MagicalRecord

final class Storage: StorageService {
    init(secrets: SecretService, expirationInterval: TimeInterval) {
        self.secrets = secrets
        let defaults = UserDefaults.standard
        stationId = Int64(defaults.integer(forKey: keyStationId))
        self.expirationInterval = expirationInterval
        self.contexts = [String:NSManagedObjectContext]()
        self.contextDates = [String:Date]()
    }

    // MARK: - common funs

    internal func safeString(_ str: String?) -> String {
        if let s = str {
            return s
        }
        return ""
    }

    // MARK: - Contexts

    internal func getContext() -> NSManagedObjectContext {
        if Thread.isMainThread {
            return NSManagedObjectContext.mr_default()
        } else {
            let id = threadId()
            return get(context: id)
        }
    }

    internal func get(context id: String)  -> NSManagedObjectContext {
        let ctx: NSManagedObjectContext
        lock.lock()
        // update/add date of context expiration
        contextDates[id] = Date(timeIntervalSinceNow: const.expireContext)
        if let c = contexts[id] {
            ctx = c
        } else {
            let coordinator = NSPersistentStoreCoordinator.mr_default()!
            ctx = NSManagedObjectContext.mr_context(with: coordinator)
            contexts[id] = ctx
        }
        lock.unlock()
        DispatchQueue(label: "ctx-background").async { [weak self] in
            self?.garbageCollectionForSavedContexts()
        }
        return ctx
    }

    internal func garbageCollectionForSavedContexts() {
        var keys = [String]()
        lock.lock()
        for (k, ctx) in contextDates {
            if ctx < Date() {
                keys.append(k)
            }
        }
        for k in keys {
            contextDates.removeValue(forKey: k)
            contexts.removeValue(forKey: k)
        }
        lock.unlock()
    }

    // MARK: - Ids

    internal func saveIds() {
        let defaults = UserDefaults.standard
        lock.lock()
        defaults.set(NSNumber(integerLiteral:Int(stationId)), forKey: keyStationId)
        lock.unlock()
        defaults.synchronize()
    }

    deinit {
        saveIds()
    }

    // MARK: - const + members

    internal let secrets: SecretService
    internal let keyStationId = "key_station_id"
    internal var stationId: Int64
    internal let lock = NSLock()
    internal let expirationInterval: TimeInterval
    internal var contexts: [String:NSManagedObjectContext]
    internal var contextDates: [String:Date]

    struct const {
        static let kSeparator: Character = ";"
        static let expireContext: TimeInterval = 5
    }
}
