//
//  CWebPage+CoreDataProperties.swift
//  benzo
//
//  Created by Michael Artuerhof on 29.08.18.
//  Copyright © 2018 MLN Group. All rights reserved.
//
//

import Foundation
import CoreData


extension CWebPage {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CWebPage> {
        return NSFetchRequest<CWebPage>(entityName: "CWebPage")
    }

    @NSManaged public var id: String?
    @NSManaged public var date: NSDate?
    @NSManaged public var content: String?

}
