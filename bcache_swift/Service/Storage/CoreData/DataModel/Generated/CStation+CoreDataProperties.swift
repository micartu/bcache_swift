//
//  CStation+CoreDataProperties.swift
//  benzo
//
//  Created by Michael Artuerhof on 17.05.18.
//  Copyright © 2018 MLN Group. All rights reserved.
//
//

import Foundation
import CoreData


extension CStation {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CStation> {
        return NSFetchRequest<CStation>(entityName: "CStation")
    }

    @NSManaged public var address: String?
    @NSManaged public var columnCount: Int16
    @NSManaged public var descr: String?
    @NSManaged public var id: Int64
    @NSManaged public var imgBig: String?
    @NSManaged public var imgSmall: String?
    @NSManaged public var lat: Double
    @NSManaged public var lon: Double
    @NSManaged public var name: String?
    @NSManaged public var prices: String?
    @NSManaged public var inCarBuyFuel: Bool
    @NSManaged public var patch: CMapPatch?

}
