//
//  CMessage+CoreDataProperties.swift
//  benzo
//
//  Created by Michael Artuerhof on 26.09.18.
//  Copyright © 2018 MLN Group. All rights reserved.
//
//

import Foundation
import CoreData


extension CMessage {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CMessage> {
        return NSFetchRequest<CMessage>(entityName: "CMessage")
    }

    @NSManaged public var body: String?
    @NSManaged public var category: String?
    @NSManaged public var created: NSDate?
    @NSManaged public var document: String?
    @NSManaged public var id: String?
    @NSManaged public var infoLink: String?
    @NSManaged public var link: String?
    @NSManaged public var read: Bool
    @NSManaged public var subcategory: String?
    @NSManaged public var tag: String?
    @NSManaged public var title: String?
    @NSManaged public var button: String?
    @NSManaged public var user: CUser?

}
