//
//  CUser+CoreDataProperties.swift
//  benzo
//
//  Created by Michael Artuerhof on 20.02.19.
//  Copyright © 2019 MLN Group. All rights reserved.
//
//

import Foundation
import CoreData


extension CUser {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CUser> {
        return NSFetchRequest<CUser>(entityName: "CUser")
    }

    @NSManaged public var balance: Int64
    @NSManaged public var birthday: NSDate?
    @NSManaged public var card: String?
    @NSManaged public var cardBlocked: Bool
    @NSManaged public var codeword: String?
    @NSManaged public var cpa_crd_code: String?
    @NSManaged public var cpa_crd_rub: Int64
    @NSManaged public var cpa_prt_code: String?
    @NSManaged public var cpa_prt_name: String?
    @NSManaged public var cpa_prt_ref1: String?
    @NSManaged public var cpa_prt_ref2: String?
    @NSManaged public var cpa_prt_ref3: String?
    @NSManaged public var cpa_prt_subid: String?
    @NSManaged public var crd_pin: String?
    @NSManaged public var email: String?
    @NSManaged public var favFuel: String?
    @NSManaged public var lastName: String?
    @NSManaged public var middleName: String?
    @NSManaged public var name: String?
    @NSManaged public var phone: String?
    @NSManaged public var phoneNumber: String?
    @NSManaged public var points: String?
    @NSManaged public var status: String?
    @NSManaged public var menuRefreshDate: NSDate?
    @NSManaged public var locpoints: NSSet?
    @NSManaged public var menuSections: NSSet?
    @NSManaged public var messages: NSSet?
    @NSManaged public var promos: NSSet?
    @NSManaged public var transactions: NSSet?

}

// MARK: Generated accessors for locpoints
extension CUser {

    @objc(addLocpointsObject:)
    @NSManaged public func addToLocpoints(_ value: CLocPoint)

    @objc(removeLocpointsObject:)
    @NSManaged public func removeFromLocpoints(_ value: CLocPoint)

    @objc(addLocpoints:)
    @NSManaged public func addToLocpoints(_ values: NSSet)

    @objc(removeLocpoints:)
    @NSManaged public func removeFromLocpoints(_ values: NSSet)

}

// MARK: Generated accessors for menuSections
extension CUser {

    @objc(addMenuSectionsObject:)
    @NSManaged public func addToMenuSections(_ value: CMenuSection)

    @objc(removeMenuSectionsObject:)
    @NSManaged public func removeFromMenuSections(_ value: CMenuSection)

    @objc(addMenuSections:)
    @NSManaged public func addToMenuSections(_ values: NSSet)

    @objc(removeMenuSections:)
    @NSManaged public func removeFromMenuSections(_ values: NSSet)

}

// MARK: Generated accessors for messages
extension CUser {

    @objc(addMessagesObject:)
    @NSManaged public func addToMessages(_ value: CMessage)

    @objc(removeMessagesObject:)
    @NSManaged public func removeFromMessages(_ value: CMessage)

    @objc(addMessages:)
    @NSManaged public func addToMessages(_ values: NSSet)

    @objc(removeMessages:)
    @NSManaged public func removeFromMessages(_ values: NSSet)

}

// MARK: Generated accessors for promos
extension CUser {

    @objc(addPromosObject:)
    @NSManaged public func addToPromos(_ value: CPromo)

    @objc(removePromosObject:)
    @NSManaged public func removeFromPromos(_ value: CPromo)

    @objc(addPromos:)
    @NSManaged public func addToPromos(_ values: NSSet)

    @objc(removePromos:)
    @NSManaged public func removeFromPromos(_ values: NSSet)

}

// MARK: Generated accessors for transactions
extension CUser {

    @objc(addTransactionsObject:)
    @NSManaged public func addToTransactions(_ value: CTransaction)

    @objc(removeTransactionsObject:)
    @NSManaged public func removeFromTransactions(_ value: CTransaction)

    @objc(addTransactions:)
    @NSManaged public func addToTransactions(_ values: NSSet)

    @objc(removeTransactions:)
    @NSManaged public func removeFromTransactions(_ values: NSSet)

}
