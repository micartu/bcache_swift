//
//  CMapPatch+CoreDataProperties.swift
//  benzo
//
//  Created by Michael Artuerhof on 17.05.18.
//  Copyright © 2018 MLN Group. All rights reserved.
//
//

import Foundation
import CoreData


extension CMapPatch {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CMapPatch> {
        return NSFetchRequest<CMapPatch>(entityName: "CMapPatch")
    }

    @NSManaged public var id: Int32
    @NSManaged public var date: NSDate?
    @NSManaged public var stations: NSSet?

}

// MARK: Generated accessors for stations
extension CMapPatch {

    @objc(addStationsObject:)
    @NSManaged public func addToStations(_ value: CStation)

    @objc(removeStationsObject:)
    @NSManaged public func removeFromStations(_ value: CStation)

    @objc(addStations:)
    @NSManaged public func addToStations(_ values: NSSet)

    @objc(removeStations:)
    @NSManaged public func removeFromStations(_ values: NSSet)

}
