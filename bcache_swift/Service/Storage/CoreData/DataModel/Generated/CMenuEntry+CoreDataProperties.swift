//
//  CMenuEntry+CoreDataProperties.swift
//  benzo
//
//  Created by Michael Artuerhof on 20.02.19.
//  Copyright © 2019 MLN Group. All rights reserved.
//
//

import Foundation
import CoreData


extension CMenuEntry {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CMenuEntry> {
        return NSFetchRequest<CMenuEntry>(entityName: "CMenuEntry")
    }

    @NSManaged public var order: Int32
    @NSManaged public var params: String?
    @NSManaged public var title: String?
    @NSManaged public var type: Int16
    @NSManaged public var section: CMenuSection?
    @NSManaged public var style: CMenuStyle?

}
