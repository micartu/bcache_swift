//
//  CTransaction+CoreDataProperties.swift
//  benzo
//
//  Created by Michael Artuerhof on 16.01.19.
//  Copyright © 2019 MLN Group. All rights reserved.
//
//

import Foundation
import CoreData


extension CTransaction {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CTransaction> {
        return NSFetchRequest<CTransaction>(entityName: "CTransaction")
    }

    @NSManaged public var brandName: String?
    @NSManaged public var channel: String?
    @NSManaged public var convertBonusDate: NSDate?
    @NSManaged public var date: NSDate?
    @NSManaged public var discountSign: Int16
    @NSManaged public var discountValue: Int16
    @NSManaged public var discountValueCurrency: Int32
    @NSManaged public var innertype: String?
    @NSManaged public var location: String?
    @NSManaged public var points: Int32
    @NSManaged public var productName: String?
    @NSManaged public var productQuantity: Double
    @NSManaged public var rrn: String?
    @NSManaged public var status: String?
    @NSManaged public var trnComment: String?
    @NSManaged public var trnType: String?
    @NSManaged public var trnValue: Int64
    @NSManaged public var trnBB: Int32
    @NSManaged public var user: CUser?

}
