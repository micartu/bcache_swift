//
//  CMenuStyle+CoreDataProperties.swift
//  benzo
//
//  Created by Michael Artuerhof on 21.02.19.
//  Copyright © 2019 MLN Group. All rights reserved.
//
//

import Foundation
import CoreData


extension CMenuStyle {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CMenuStyle> {
        return NSFetchRequest<CMenuStyle>(entityName: "CMenuStyle")
    }

    @NSManaged public var cellHeight: Double
    @NSManaged public var fontSize: Double
    @NSManaged public var fontWeight: Int16
    @NSManaged public var offsets: String?
    @NSManaged public var color: String?
    @NSManaged public var menuEntry: CMenuEntry?
    @NSManaged public var menuSection: CMenuSection?

}
