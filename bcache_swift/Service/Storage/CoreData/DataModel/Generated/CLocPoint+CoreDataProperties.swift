//
//  CLocPoint+CoreDataProperties.swift
//  benzo
//
//  Created by Michael Artuerhof on 30.08.18.
//  Copyright © 2018 MLN Group. All rights reserved.
//
//

import Foundation
import CoreData


extension CLocPoint {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CLocPoint> {
        return NSFetchRequest<CLocPoint>(entityName: "CLocPoint")
    }

    @NSManaged public var lat: Double
    @NSManaged public var lon: Double
    @NSManaged public var user: CUser?

}
