//
//  CPromo+CoreDataProperties.swift
//  benzo
//
//  Created by Michael Artuerhof on 02.10.18.
//  Copyright © 2018 MLN Group. All rights reserved.
//
//

import Foundation
import CoreData


extension CPromo {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CPromo> {
        return NSFetchRequest<CPromo>(entityName: "CPromo")
    }

    @NSManaged public var body: String?
    @NSManaged public var dateEnd: NSDate?
    @NSManaged public var dateStart: NSDate?
    @NSManaged public var id: String?
    @NSManaged public var link: String?
    @NSManaged public var rules: String?
    @NSManaged public var title: String?
    @NSManaged public var type: String?
    @NSManaged public var sharing: String?
    @NSManaged public var user: CUser?

}
