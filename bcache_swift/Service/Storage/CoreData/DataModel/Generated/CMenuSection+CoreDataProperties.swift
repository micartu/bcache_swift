//
//  CMenuSection+CoreDataProperties.swift
//  benzo
//
//  Created by Michael Artuerhof on 20.02.19.
//  Copyright © 2019 MLN Group. All rights reserved.
//
//

import Foundation
import CoreData


extension CMenuSection {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CMenuSection> {
        return NSFetchRequest<CMenuSection>(entityName: "CMenuSection")
    }

    @NSManaged public var order: Int32
    @NSManaged public var entries: NSSet?
    @NSManaged public var style: CMenuStyle?
    @NSManaged public var user: CUser?

}

// MARK: Generated accessors for entries
extension CMenuSection {

    @objc(addEntriesObject:)
    @NSManaged public func addToEntries(_ value: CMenuEntry)

    @objc(removeEntriesObject:)
    @NSManaged public func removeFromEntries(_ value: CMenuEntry)

    @objc(addEntries:)
    @NSManaged public func addToEntries(_ values: NSSet)

    @objc(removeEntries:)
    @NSManaged public func removeFromEntries(_ values: NSSet)

}
