//
//  Storage+LocPoint.swift
//  benzo
//
//  Created by Michael Artuerhof on 05.06.19.
//  Copyright © 2019 MLN Group. All rights reserved.
//

import Foundation
import MagicalRecord

extension Storage: CacheLocPointService {
    func getLocPoints() -> [LocPoint]? {
        return getLocPoints(in: NSManagedObjectContext.mr_default())
    }

    func add(locpoints: [LocPoint], completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            if let u = self.getUser(self.secrets.user, in: context) {
                for p in locpoints {
                    self.add(locpoint: p, into: u, in: context)
                }
            }
        }, completion: { (success, err) in
                completion()
        })
    }

    func add(locpoint p: LocPoint, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            if let u = self.getUser(self.secrets.user, in: context) {
                self.add(locpoint: p, into: u, in: context)
            }
        }, completion: { (success, err) in
                completion()
        })
    }

    func deleteAllLocPoints(completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            if let u = self.getUser(self.secrets.user, in: context) {
                self.deleteAllLocPoints(for: u, in: context)
            }
        }, completion: { (success, err) in
                completion()
        })
    }

    // MARK: - Private/Internal

    private func getLocPoints(in context: NSManagedObjectContext) -> [LocPoint]? {
        if let u = getUser(secrets.user, in: context) {
            var pp = [LocPoint]()
            if let locpoints = u.locpoints as? Set<CLocPoint> {
                for p in locpoints {
                    let converted = convert(locpoint: p)
                    pp.append(converted)
                }
            }
            return pp
        }
        return nil
    }

    private func add(locpoint p: LocPoint, into u: CUser, in context: NSManagedObjectContext) {
        if let pp = CLocPoint.mr_createEntity(in: context) {
            pp.lat = p.lat
            pp.lon = p.lon
            pp.user = u
            u.addToLocpoints(pp)
        }
    }

    private func convert(locpoint p: CLocPoint) -> LocPoint {
        return LocPoint(lat: p.lat, lon: p.lon)
    }

    internal func deleteAllLocPoints(for u: CUser, in context: NSManagedObjectContext) {
        if let pp = u.locpoints as? Set<CLocPoint> {
            u.removeFromLocpoints(pp as NSSet)
            for p in pp {
                p.mr_deleteEntity(in: context)
            }
        }
    }
}
