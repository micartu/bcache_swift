//
//  Storage+User.swift
//  benzo
//
//  Created by Michael Artuerhof on 05.06.19.
//  Copyright © 2019 MLN Group. All rights reserved.
//

import Foundation
import MagicalRecord

extension Storage: CacheUserService {
    func exists(user: String) -> Bool {
        let predicate = NSPredicate(format: "phone == %@", user)
        if CUser.mr_countOfEntities(with: predicate, in: getContext()) > 0 {
            return true
        }
        return false
    }

    func getCurrentUser() -> User? {
        return get(user: secrets.user)
    }

    func get(user: String) -> User? {
        return get(user: user, in: getContext())
    }

    func create(user: User, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            self.create(user: user, in: context)
        }, completion: { (success, err) in
                completion()
        })
    }

    func update(user: User, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            if let u = self.getUser(user.login, in: context) {
                self.update(user: u, from: user)
            } else {
                self.create(user: user, in: context)
            }
        }, completion: { success, err in
                completion()
        })
    }

    func delete(user u: String, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            let predicate = NSPredicate(format: "phone == %@", u)
            if let users = CUser.mr_findAll(with: predicate, in: context) as? [CUser] {
                for u in users {
                    self.deleteAllTransactions(for: u, in: context)
                    self.deleteAllMessages(for: u, in: context)
                    self.deleteAllPromos(for: u, in: context)
                    self.deleteAllLocPoints(for: u, in: context)
                    self.deleteAllMenuSections(for: u, in: context)
                    let curUser = self.secrets.user
                    if u.phone == curUser {
                        self.secrets.erase()
                    }
                    u.mr_deleteEntity(in: context)
                }
            }
        }, completion: { (success, err) in
            completion()
        })
    }

    // MARK: - Private/Internal

    // MARK: - getters

    internal func get(user: String, in context: NSManagedObjectContext) -> User? {
        if let u = getUser(user, in: context) {
            return convert(user: u)
        }
        return nil
    }

    internal func getUser(_ user: String, in context: NSManagedObjectContext) -> CUser? {
        let predicate = NSPredicate(format: "phone == %@", user)
        return CUser.mr_findFirst(with: predicate, in: context)
    }

    internal func getUser(_ user: String) -> CUser? {
        return getUser(user, in: getContext())
    }

    // MARK: - create

    internal func create(user: User, in context: NSManagedObjectContext) {
        let u = CUser.mr_createEntity(in: context)!
        update(user: u, from: user)
    }

    // MARK: - converting

    internal func convert(user u: CUser) -> User {
        let card = u.card ?? ""
        let dcard = secrets.decrypt(card)
        let loyality = UserLoyality(cpa_crd_code: u.cpa_crd_code ?? "",
                                    cpa_crd_rub: Int(u.cpa_crd_rub),
                                    cpa_prt_code: u.cpa_prt_code ?? "",
                                    cpa_prt_name: u.cpa_prt_name ?? "",
                                    cpa_prt_ref1: u.cpa_prt_ref1 ?? "",
                                    cpa_prt_ref2: u.cpa_prt_ref2 ?? "",
                                    cpa_prt_ref3: u.cpa_prt_ref3 ?? "",
                                    cpa_prt_subid: u.cpa_prt_subid ?? "")
        let spin: String
        if let pin = u.crd_pin {
            let sep = pin.split(separator: "|")
            if sep.count > 1 {
                spin = pin
            } else if !pin.contains("|") {
                spin = "0|\(pin)"
            } else {
                spin = pin
            }
        } else {
            spin = "1|0000"
        }
        let usr = User(login: u.phone!,
                       phone: u.phoneNumber ?? "",
                       name: u.name ?? "",
                       surname: u.lastName ?? "",
                       midname: u.middleName ?? "",
                       avatar: "",
                       fuel: u.favFuel ?? "",
                       codeword: u.codeword ?? "",
                       card: dcard,
                       pin: spin,
                       email: u.email!,
                       balance: Double(u.balance),
                       loyality: loyality,
                       cardBlocked: u.cardBlocked,
                       apn: "")
        return usr
    }

    internal func update(user cu: CUser, from u: User) {
        cu.balance = Int64(u.balance)
        cu.email = u.email
        cu.lastName = u.surname
        cu.middleName = u.midname
        cu.card = secrets.encrypt(u.card)
        cu.name = u.name
        cu.phone = u.login
        cu.phoneNumber = u.phone
        cu.codeword = u.codeword
        cu.favFuel = u.fuel
        cu.cardBlocked = u.cardBlocked
        cu.crd_pin = u.pin
        // loyality
        cu.cpa_crd_code = u.loyality.cpa_crd_code
        cu.cpa_crd_rub = Int64(u.loyality.cpa_crd_rub)
        cu.cpa_prt_code = u.loyality.cpa_prt_code
        cu.cpa_prt_name = u.loyality.cpa_prt_name
        cu.cpa_prt_ref1 = u.loyality.cpa_prt_ref1
        cu.cpa_prt_ref2 = u.loyality.cpa_prt_ref2
        cu.cpa_prt_ref3 = u.loyality.cpa_prt_ref3
        cu.cpa_prt_subid = u.loyality.cpa_prt_subid

        // update avatar in UserDefaults
        let def = UserDefaults.standard
        def.set(u.avatar, forKey: Const.keys.avatarPath)
        def.synchronize()
    }
}
