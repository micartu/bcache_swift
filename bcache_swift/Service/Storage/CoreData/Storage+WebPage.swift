//
//  Storage+WebPage.swift
//  benzo
//
//  Created by Michael Artuerhof on 05.06.19.
//  Copyright © 2019 MLN Group. All rights reserved.
//

import Foundation
import MagicalRecord

extension Storage: CacheWebPageService {
    func get(webpage id: String) -> WebPage? {
        if let p = get(webpage: id, in: getContext()) {
            return convert(webpage: p)
        }
        return nil
    }

    func update(webpage: WebPage, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            if let p = self.get(webpage: webpage.id, in: context) {
                self.update(webpage: p, from: webpage)
            } else {
                self.create(webpage: webpage, in: context)
            }
        }, completion: { success, err in
                completion()
        })
    }

    func create(webpage page: WebPage, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            self.create(webpage: page, in: context)
        }, completion: { (success, err) in
                completion()
        })
    }

    func delete(webpage p: WebPage, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            self.delete(webpage: p, in: context)
        }, completion: { (success, err) in
                completion()
        })
    }

    // MARK: - Private/Internal

    private func get(webpage id: String, in context: NSManagedObjectContext) -> CWebPage? {
        let predicate = NSPredicate(format: "id == %@", id)
        return CWebPage.mr_findFirst(with: predicate, in: context)
    }

    private func create(webpage: WebPage, in context: NSManagedObjectContext) {
        let p = CWebPage.mr_createEntity(in: context)!
        p.id = webpage.id
        update(webpage: p, from: webpage)
    }

    private func update(webpage p: CWebPage, from u: WebPage) {
        p.content = u.content
    }

    private func convert(webpage p: CWebPage) -> WebPage {
        return WebPage(id: p.id!,
                       content: p.content!)
    }
}
