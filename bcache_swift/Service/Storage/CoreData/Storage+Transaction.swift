//
//  Storage+Transaction.swift
//  benzo
//
//  Created by Michael Artuerhof on 05.06.19.
//  Copyright © 2019 MLN Group. All rights reserved.
//

import Foundation
import MagicalRecord

extension Storage: CacheTransactionService {
    func exists(transaction: String) -> Bool {
        let predicate = getPredicateFor(id: transaction)
        if CTransaction.mr_countOfEntities(with: predicate, in: getContext()) > 0 {
            return true
        }
        return false
    }

    func getTransaction(rrn: String) -> Transaction? {
        let predicate = getPredicateFor(id: rrn)
        let tra = getTransactions(with: predicate, page: 0, count: 1)
        if tra.count > 0 {
            return tra.first
        }
        return nil
    }

    func getTransactions(from startDate: NSDate?, page: Int, count: Int) -> [Transaction] {
        return getTransactions(stringFormat: "",
                               args: nil,
                               from: startDate,
                               page: page,
                               count: count)
    }

    func getTransactions(of type: String, from startDate: NSDate?, page: Int, count: Int) -> [Transaction] {
        return getTransactions(stringFormat: "(innertype == %@)",
                               args: [type],
                               from: startDate,
                               page: page,
                               count:count)
    }

    func create(transaction tr: Transaction, completion: @escaping (() -> Void)) {
        create(transactions: [tr], completion: completion)
    }

    func create(transactions trs: [Transaction], completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            for tr in trs {
                self.create(transaction: tr, in: context)
            }
        }, completion: { (success, err) in
                completion()
        })
    }

    func update(transaction: Transaction, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            if let t = self.get(transaction: transaction.rrn, in: context) {
                self.update(transaction: t, from: transaction)
            } else {
                self.create(transaction: transaction, in: context)
            }
        }, completion: { success, err in
                completion()
        })
    }

    func delete(transaction rrn: String, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ context in
            let predicate = NSPredicate(format: "rrn == %@", rrn)
            if let t = CTransaction.mr_findFirst(with: predicate, in: context) {
                if let u = t.user {
                    u.removeFromTransactions(t)
                }
                t.mr_deleteEntity(in: context)
            }
        }, completion: { (success, err) in
            completion()
        })
    }

    // MARK: - Private/Internal

    private func getTransactions(with predicate: NSPredicate, page: Int, count: Int) -> [Transaction] {
        let request = CTransaction.mr_requestAllSorted(by: "date", ascending: false, with: predicate)
        request.fetchOffset = page * count
        request.fetchLimit = count
        var out = [Transaction]()
        if let tra = CTransaction.mr_executeFetchRequest(request) as? [CTransaction] {
            for t in tra {
                let tt = convert(transaction: t)
                out.append(tt)
            }
        }
        return out
    }

    private func getTransactions(stringFormat: String, args: [Any]?, from startDate: NSDate?, page: Int, count: Int) -> [Transaction] {
        var format = ""
        if stringFormat.count > 0 {
            format += stringFormat + " AND "
        }
        format += "(user.phone == %@)"
        let start: NSDate
        if let sd = startDate {
            format += " AND (date >= %@)"
            start = sd
        } else {
            start = NSDate()
        }
        let argToPass: [Any]
        if let a = args {
            argToPass = a + [secrets.user, start]
        } else {
            argToPass = [secrets.user, start]
        }
        let predicate = NSPredicate(format: format, argumentArray: argToPass)
        return getTransactions(with: predicate, page: page, count: count)
    }

    private func getPredicateFor(id: String) -> NSPredicate {
        return NSPredicate(format: "(user.phone == %@) AND (rrn == %@)", secrets.user, id)
    }

    private func get(transaction id: String, in context: NSManagedObjectContext) -> CTransaction? {
        let predicate = getPredicateFor(id: id)
        return CTransaction.mr_findFirst(with: predicate, in: context)
    }

    private func create(transaction: Transaction, in context: NSManagedObjectContext) {
        let t = CTransaction.mr_createEntity(in: context)!
        t.rrn = transaction.rrn
        update(transaction: t, from: transaction)
        if let u = getUser(secrets.user, in: context) {
            t.user = u
            u.addToTransactions(t)
        }
    }

    private func convert(transaction t: CTransaction) -> Transaction {
        return Transaction(rrn: t.rrn!,
                           date: t.date!,
                           trnType: t.trnType!,
                           brandName: t.brandName!,
                           location: t.location!,
                           productName: t.productName!,
                           productQuantity: t.productQuantity,
                           trnBB: Int(t.trnBB),
                           trnValue: Int(t.trnValue),
                           points: Int(t.points),
                           status: t.status!,
                           trnComment: t.trnComment!,
                           convertBonusDate: t.convertBonusDate!,
                           discountSign: Int(t.discountSign),
                           discountValue: Int(t.discountValue),
                           discountValueCurrency: Int(t.discountValueCurrency),
                           channel: t.channel!,
                           innertype: t.innertype ?? "")
    }

    private func update(transaction ct: CTransaction, from t: Transaction) {
        ct.date = t.date
        ct.trnType = t.trnType
        ct.innertype = t.innertype
        ct.brandName = t.brandName
        ct.location = t.location
        ct.productName = t.productName
        ct.productQuantity = t.productQuantity
        ct.trnValue = Int64(t.trnValue)
        ct.points = Int32(t.points)
        ct.trnBB = Int32(t.trnBB)
        ct.status = t.status
        ct.trnComment = t.trnComment
        ct.convertBonusDate = t.convertBonusDate
        ct.discountSign = Int16(t.discountSign)
        ct.discountValue = Int16(t.discountValue)
        ct.discountValueCurrency = Int32(t.discountValueCurrency)
        ct.channel = t.channel
    }
}
