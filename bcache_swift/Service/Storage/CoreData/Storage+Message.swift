//
//  Storage+Message.swift
//  benzo
//
//  Created by Michael Artuerhof on 05.06.19.
//  Copyright © 2019 MLN Group. All rights reserved.
//

import Foundation
import MagicalRecord

extension Storage: CacheMessageService {
    func getMessagesForCurrentUser(category: String) -> [Message]? {
        return getMessagesForCurrentUser(linked: "", category: category)
    }

    func getMessagesForCurrentUser(linked l: String, category: String) -> [Message]? {
        var format = "(user.phone == %@) AND (link == %@)"
        if category.count > 0 {
            format += "AND (category == %@)"
        }
        let predicate = NSPredicate(format: format, secrets.user, l, category)
        if let msgs = CMessage.mr_findAllSorted(by: "created",
                                                ascending: false,
                                                with: predicate,
                                                in: getContext()) as? [CMessage] {
            var out = [Message]()
            for m in msgs {
                let mm = convert(message: m)
                out.append(mm)
                // if we're looking not for messages on the root leve
                // and want to fetch all answers to the messages
                // we've already added then try to attach messages linked to this one
                if l.count > 0 {
                    if let msgsToM = getMessagesForCurrentUser(linked: mm.id, category: category) {
                        out.append(contentsOf: msgsToM)
                    }
                }
            }
            return out
        }
        return nil
    }

    func update(message m: Message, completion: @escaping (() -> Void)) {
        update(messages: [m], completion: completion)
    }

    func update(messages msg: [Message], completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            for m in msg {
                if let cm = self.get(message: m.id, in: context) {
                    self.update(message: cm, from: m)
                } else {
                    self.create(message: m, in: context)
                }
            }
        }, completion: { success, err in
                completion()
        })
    }

    func create(message m: Message, completion: @escaping (() -> Void)) {
        create(messages: [m], completion: completion)
    }

    func create(messages msgs: [Message], completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            for m in msgs {
                self.create(message: m, in: context)
            }
        }, completion: { (success, err) in
                completion()
        })
    }

    func deleteAllMessages(completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            if let u = self.getUser(self.secrets.user, in: context) {
                self.deleteAllMessages(for: u, in: context)
            }
        }, completion: { (success, err) in
                completion()
        })
    }

    // MARK: - Private/Internal

    private func create(message: Message, in context: NSManagedObjectContext) {
        let m = CMessage.mr_createEntity(in: context)!
        m.id = message.id
        update(message: m, from: message)
        if let u = getUser(secrets.user, in: context) {
            m.user = u
            u.addToMessages(m)
        }
    }

    private func get(message id: String, in context: NSManagedObjectContext) -> CMessage? {
        let predicate = NSPredicate(format: "(user.phone == %@) AND (id == %@)", secrets.user, id)
        return CMessage.mr_findFirst(with: predicate, in: context)
    }

    private func update(message m: CMessage, from mes: Message) {
        m.title = mes.title
        m.body = mes.body
        m.created = mes.created
        m.document = mes.document
        m.infoLink = mes.infoLink
        m.link = mes.link
        m.button = mes.button
        m.category = mes.category
        m.subcategory = mes.subcategory
        m.tag = mes.tag
        if !m.read { // this flag could be changed only once
            m.read = mes.read
        }
    }

    private func convert(message m: CMessage) -> Message {
        return Message(id: m.id!,
                       title: m.title!,
                       body: m.body!,
                       created: m.created!,
                       infoLink: m.infoLink ?? "",
                       link: m.link!,
                       read: m.read,
                       subcategory: m.subcategory!,
                       tag: m.tag!,
                       button: m.button ?? "",
                       document: m.document ?? "",
                       category: m.category!)
    }
}
