//
//  Storage+Menu.swift
//  benzo
//
//  Created by Michael Artuerhof on 05.06.19.
//  Copyright © 2019 MLN Group. All rights reserved.
//

import Foundation
import MagicalRecord

extension Storage: CacheMenuService {
    func getStoredMenuSectionsForCurrentUser() -> [MenuSection]? {
        if let u = getUser(secrets.user) {
            if let css = u.menuSections as? Set<CMenuSection> {
                let csa = css.sorted(by: {$0.order < $1.order})
                var sections = [MenuSection]()
                for cs in csa {
                    let s = convert(menuSection: cs)
                    sections.append(s)
                }
                return sections
            }
        }
        return nil
    }

    func storedMenuSectionsForCurrentUserExpired() -> Bool {
        if let u = getUser(secrets.user) {
            if u.menuSections != nil {
                if let d = u.menuRefreshDate as Date? {
                    let date = Date() // current date
                    if date < d.addingTimeInterval(expirationInterval) {
                        return false
                    }
                }
            }
        }
        return true
    }

    func storeForCurrentUser(menuSections ms: [MenuSection], completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            if let u = self.getUser(self.secrets.user, in: context) {
                self.deleteAllMenuSections(for: u, in: context)
                var order: Int32 = 1
                var lastUsedOrders = [Int32]()
                let csections = NSMutableSet(capacity: ms.count)
                for s in ms {
                    let cmsection = CMenuSection.mr_createEntity(in: context)!
                    let centries = NSMutableSet(capacity: s.entries.count)
                    if s.order != 0 {
                        order = Int32(s.order)
                    } else {
                        lastUsedOrders.append(order)
                    }
                    order = self.check(lastUsed: &lastUsedOrders, order: order)
                    cmsection.order = order
                    for m in s.entries {
                        let cm = CMenuEntry.mr_createEntity(in: context)!
                        self.update(menuentry: cm, from: m)
                        if m.order == 0 {
                            cm.order = order
                            order += 1
                        }
                        if let s = m.style {
                            let cs = self.create(menuStyle: s, in: context)
                            self.update(style: cs, from: s)
                            cm.style = cs
                        }
                        centries.add(cm)
                    }
                    cmsection.entries = centries
                    cmsection.style = self.create(menuStyle: s.style, in: context)
                    csections.add(cmsection)
                    order += 1
                }
                u.menuSections = csections
                u.menuRefreshDate = NSDate()
            }
            }, completion: { (success, err) in
                completion()
        })
    }

    func deleteAllMenuSectionsForCurrentUser(completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            if let u = self.getUser(self.secrets.user, in: context) {
                self.deleteAllMenuSections(for: u, in: context)
            }
        }, completion: { (success, err) in
                completion()
        })
    }

    // MARK: - Private/Internal

    private func check(lastUsed: inout [Int32], order: Int32, append: Bool = false) -> Int32 {
        if lastUsed.contains(order) {
            return check(lastUsed: &lastUsed, order: order + 1, append: true)
        } else {
            if append {
                lastUsed.append(order)
            }
            return order
        }
    }

    private func create(menuStyle s: MenuStyle, in context: NSManagedObjectContext) -> CMenuStyle {
        let cs = CMenuStyle.mr_createEntity(in: context)!
        update(style: cs, from: s)
        return cs
    }

    private func update(menuentry cm: CMenuEntry, from m: MenuEntry) {
        cm.type = Int16(m.type.rawValue)
        cm.title = m.title
        cm.order = Int32(m.order)
        cm.params = m.params
        // style must be updated somewhere else
    }

    private func update(style cs: CMenuStyle, from s: MenuStyle) {
        cs.cellHeight = s.cellHeight
        cs.fontSize = s.fontSize
        if let c = s.fontColor {
            let (r,g,b,a) = c.rgbComponents
            let (ri,gi,bi,ai) = (Int(r*255), Int(g*255), Int(b*255), Int(a*255))
            cs.color = String(format:"#%02X%02X%02X%02X", ri, gi, bi, ai)
        } else {
            cs.color = nil
        }
        cs.fontWeight = Int16(s.fontWeight.rawValue)
        cs.offsets = "\(s.soffsets.top)\(const.kSeparator)\(s.soffsets.bottom)"
    }

    internal func deleteAllMenuSections(for u: CUser, in context: NSManagedObjectContext) {
        if let ss = u.menuSections as? Set<CMenuSection> {
            u.removeFromMenuSections(ss as NSSet)
            for s in ss {
                if let mm = s.entries as? Set<CMenuEntry> {
                    for m in mm {
                        if let style = m.style {
                            style.mr_deleteEntity(in: context)
                        }
                        m.mr_deleteEntity(in: context)
                    }
                }
                if let style = s.style {
                    style.mr_deleteEntity(in: context)
                }
                s.mr_deleteEntity(in: context)
            }
        }
        u.menuSections = nil
        u.menuRefreshDate = nil
        // time to load a new menu
        delay(0.5) {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: Const.keys.notifRefreshMenu),
                                            object: nil,
                                            userInfo: nil)
        }
    }

    private func convert(menuSection m: CMenuSection) -> MenuSection {
        var menus = [MenuEntry]()
        if let mm = m.entries as? Set<CMenuEntry> {
            let smm = mm.sorted(by: {$0.order < $1.order})
            for cm in smm {
                let m = convert(menuEntry: cm)
                menus.append(m)
            }
        }
        let style: MenuStyle
        if let cs = m.style {
            style = convert(menuStyle: cs)
        } else {
            style = styleOf(fontSize: 12)
        }
        return MenuSection(entries: menus,
                           order: Int(m.order),
                           style: style)
    }

    private func convert(menuEntry m: CMenuEntry) -> MenuEntry {
        let type: menuType
        if let t = menuType(rawValue: Int(m.type)) {
            type = t
        } else {
            type = .offer
        }
        let style: MenuStyle?
        if let cs = m.style {
            style = convert(menuStyle: cs)
        } else {
            style = nil
        }
        return MenuEntry(type: type,
                         order: Int(m.order),
                         title: m.title!,
                         params: m.params!,
                         style: style)
    }

    private func convert(menuStyle s: CMenuStyle) -> MenuStyle {
        let top: Double
        let bottom: Double
        let offsets = s.offsets!.split(separator: const.kSeparator).map({String($0)})
        if offsets.count > 1 {
            top = offsets[0].toDouble
            bottom = offsets[1].toDouble
        } else {
            top = 0
            bottom = 0
        }
        let weight: menuFontWeight
        if let w = menuFontWeight(rawValue: Int(s.fontWeight)) {
            weight = w
        } else {
            weight = .normal
        }
        let color: UIColor?
        if let c = s.color {
            color = UIColor(hexString: c)
        } else {
            color = nil
        }
        return MenuStyle(cellHeight: s.cellHeight,
                         fontSize: s.fontSize,
                         fontColor: color,
                         fontWeight: weight,
                         soffsets: menuSectionOffset(top: top,
                                                     bottom: bottom))
    }
}
