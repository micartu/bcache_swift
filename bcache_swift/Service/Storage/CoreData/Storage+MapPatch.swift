//
//  Storage+MapPatch.swift
//  benzo
//
//  Created by Michael Artuerhof on 05.06.19.
//  Copyright © 2019 MLN Group. All rights reserved.
//

import Foundation
import MagicalRecord

extension Storage: CacheMapPatchService {
    func exists(patch id: Int) -> Bool {
        return exists(patch: id, in: getContext())
    }

    func isExpired(patch id: Int, expireInterval: TimeInterval, on date: Date) -> Bool {
        return isExpired(patch: id,
                         expireInterval: expireInterval,
                         on: date,
                         in: getContext())
    }

    func get(patch id: Int) -> MapPatchBase? {
        return getMapPatchBase(patch: id, in: getContext())
    }

    func getStationsIn(patch id: Int) -> [Station] {
        return getStationsIn(patch: id, in: getContext())
    }

    func create(patch id: Int, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            self.create(patch: id, in: context)
        }, completion: { (success, err) in
                completion()
        })
    }

    func delete(patch p: Int, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ context in
            let predicate = NSPredicate(format: "id == %d", p)
            if let mp = CMapPatch.mr_findFirst(with: predicate, in: context) {
                if let stations = mp.stations as? Set<CStation> {
                    mp.removeFromStations(stations as NSSet)
                    for s in stations {
                        s.mr_deleteEntity(in: context)
                    }
                }
                mp.mr_deleteEntity(in: context)
            }
        }, completion: { (success, err) in
            completion()
        })
    }

    // MARK: - Private/Internal

    private func isExpired(patch id: Int,
                            expireInterval: TimeInterval,
                            on date: Date,
                            in context: NSManagedObjectContext) -> Bool {
        if let p = get(patch: id, in: context) {
            if let d = p.date as Date? {
                if date > d.addingTimeInterval(expireInterval) {
                    return true
                }
            }
        }
        return false
    }

    // MARK: - getters

    private func getMapPatchBase(patch id: Int, in context: NSManagedObjectContext) -> MapPatchBase? {
        if let p = get(patch: id, in: context) {
            return convert(patch: p)
        }
        return nil
    }

    private func get(patch id: Int, in context: NSManagedObjectContext) -> CMapPatch? {
        let predicate = NSPredicate(format: "id == %d", id)
        return CMapPatch.mr_findFirst(with: predicate, in: context)
    }

    internal func getOrCreate(patch id: Int, in context: NSManagedObjectContext) -> CMapPatch {
        let p: CMapPatch
        if let pp = get(patch: id, in: context) {
            p = pp
        } else {
            p = create(patch: id, in: context)
        }
        return p
    }

    internal func get(patch id: Int, from ids: inout [CMapPatch], in context: NSManagedObjectContext) -> CMapPatch {
        for p in ids {
            if p.id == id {
                return p
            }
        }
        let p = getOrCreate(patch: id, in: context)
        ids.append(p)
        return p
    }

    // MARK: - create

    @discardableResult
    internal func create(patch id: Int, in context: NSManagedObjectContext) -> CMapPatch {
        let p = CMapPatch.mr_createEntity(in: context)!
        p.id = Int32(id)
        p.date = NSDate()
        return p
    }

    // MARK: - exists / convert

    private func exists(patch id: Int, in context: NSManagedObjectContext) -> Bool {
        let predicate = NSPredicate(format: "id == %d", id)
        if CMapPatch.mr_countOfEntities(with: predicate, in: context) > 0 {
            return true
        }
        return false
    }

    internal func convert(patch p: CMapPatch) -> MapPatchBase {
        var stations = [Station]()
        if let st = p.stations as? Set<CStation> {
            for cs in st {
                let s = convert(station: cs)
                stations.append(s)
            }
        }
        return MapPatchBase(id: Int(p.id),
                            date: (p.date as Date?) ?? Date(),
                            stations: stations)
    }
}
