//
//  Storage+Promo.swift
//  benzo
//
//  Created by Michael Artuerhof on 05.06.19.
//  Copyright © 2019 MLN Group. All rights reserved.
//

import Foundation
import MagicalRecord

extension Storage: CachePromoService {
    func exists(promo: String) -> Bool {
        let predicate = getPredicateFor(promo: promo)
        if CPromo.mr_countOfEntities(with: predicate, in: getContext()) > 0 {
            return true
        }
        return false
    }

    func getPromosForCurrentUser() -> [Promo]? {
        let predicate = NSPredicate(format: "(user.phone == %@)", secrets.user)
        if let prms = CPromo.mr_findAllSorted(by: "dateEnd", ascending: false, with: predicate) as? [CPromo] {
            var out = [Promo]()
            for p in prms {
                let pp = convert(promo: p)
                out.append(pp)
            }
            return out
        }
        return nil
    }

    func create(promo p: Promo, completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            self.create(promo: p, in: context)
        }, completion: { (success, err) in
                completion()
        })
    }

    func update(promos prms: [Promo], completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            for p in prms {
                if let pp = self.get(promo: p.id, in: context) {
                    self.update(promo: pp, from: p)
                } else {
                    self.create(promo: p, in: context)
                }
            }
            }, completion: { success, err in
                completion()
        })
    }

    func deleteAllPromos(completion: @escaping (() -> Void)) {
        MagicalRecord.save({ [unowned self] context in
            if let u = self.getUser(self.secrets.user, in: context) {
                self.deleteAllPromos(for: u, in: context)
            }
        }, completion: { (success, err) in
                completion()
        })
    }

    // MARK: - Private/Internal

    private func create(promo: Promo, in context: NSManagedObjectContext) {
        if let u = self.getUser(self.secrets.user, in: context) {
            let p = CPromo.mr_createEntity(in: context)!
            p.id = promo.id
            p.user = u
            u.addToPromos(p)
            update(promo: p, from: promo)
        }
    }

    private func getPredicateFor(promo id: String) -> NSPredicate {
        return NSPredicate(format: "(user.phone == %@) AND (id == %@)", secrets.user, id)
    }

    private func get(promo id: String, in context: NSManagedObjectContext) -> CPromo? {
        let predicate = getPredicateFor(promo: id)
        return CPromo.mr_findFirst(with: predicate, in: context)
    }

    private func update(promo p: CPromo, from pr: Promo) {
        p.dateStart = pr.dateStart
        p.dateEnd = pr.dateEnd
        p.title = pr.title
        p.type = pr.type.rawValue
        p.body = pr.body
        p.sharing = pr.sharing
        p.link = pr.link
        p.rules = pr.rules
    }

    private func convert(promo p: CPromo) -> Promo {
        let type: promoType
        if let t = promoType(rawValue: p.type!) {
            type = t
        } else {
            type = .unknown
        }
        return Promo(id: p.id!,
                     type: type,
                     dateStart: p.dateStart!,
                     dateEnd: p.dateEnd!,
                     title: p.title!,
                     body: p.body!,
                     sharing: p.sharing ?? "",
                     link: p.link!,
                     rules: p.rules!)
    }
}
