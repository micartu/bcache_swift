//
//  CacheMiscService.swift
//  benzo
//
//  Created by Michael Artuerhof on 06.06.19.
//  Copyright © 2019 MLN Group. All rights reserved.
//

import Foundation

protocol CacheMiscService {
    func onBoarded() -> Bool
    func authenticated() -> Bool
}
