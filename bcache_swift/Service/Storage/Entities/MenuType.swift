//
//  MenuType.swift
//  bcache_swift
//
//  Created by Michael Artuerhof on 19.06.19.
//  Copyright © 2019 Michael Artuerhof. All rights reserved.
//

import Foundation

enum menuType: Int {
    case map
    case addMoney
    case pay
    case channels
    case history
    case profile
    case bankCards
    case messages
    case promo
    case logout
    case separator
    case support
    case offer
    case policy
    case scanner
    case statistics
    case account
    case promoCode
    case about
    case addCard
}
