//
//  LocPoint.swift
//  benzo
//
//  Created by Michael Artuerhof on 30.08.18.
//  Copyright © 2018 MLN Group. All rights reserved.
//

import Foundation

class LocPoint: NSObject, NSCoding, Codable {
    let lat: Double
    let lon: Double

    init(lat: Double, lon: Double) {
        self.lat = lat
        self.lon = lon
    }

    // MARK: NSCoding

    required convenience init?(coder aDecoder: NSCoder) {
        let lat = aDecoder.decodeDouble(forKey: "lat")
        let lon = aDecoder.decodeDouble(forKey: "lon")
        self.init(lat: lat, lon: lon)
    }

    func encode(with aCoder: NSCoder) {
        aCoder.encode(lat, forKey: "lat")
        aCoder.encode(lon, forKey: "lon")
    }
}
