//
//  MenuStyle.swift
//  benzo
//
//  Created by Michael Artuerhof on 19.02.19.
//  Copyright © 2019 MLN Group. All rights reserved.
//

import Foundation

enum menuFontWeight: Int {
    case bold
    case normal
    case medium
    case light
}

struct menuSectionOffset {
    let top: Double
    let bottom: Double
}

struct MenuStyle {
    let cellHeight: Double
    let fontSize: Double
    let fontColor: UIColor?
    let fontWeight: menuFontWeight
    let soffsets: menuSectionOffset
}
