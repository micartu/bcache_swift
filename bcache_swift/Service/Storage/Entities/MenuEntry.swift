//
//  MenuEntry.swift
//  benzo
//
//  Created by Michael Artuerhof on 19.02.19.
//  Copyright © 2019 MLN Group. All rights reserved.
//

import Foundation

struct MenuEntry {
    let type: menuType
    let order: Int
    let title: String
    let params: String
    let style: MenuStyle?
}
