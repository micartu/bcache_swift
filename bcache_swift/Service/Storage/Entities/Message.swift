//
//  Message.swift
//  benzo
//
//  Created by Michael Artuerhof on 26.09.18.
//  Copyright © 2018 MLN Group. All rights reserved.
//

import Foundation

struct Message {
    let id: String
    let title: String
    let body: String
    let created: NSDate
    let infoLink: String
    let link: String
    let read: Bool
    let subcategory: String
    let tag: String
    let button: String
    let document: String
    let category: String
}

func flipReadOn(message m: Message) -> Message {
    return Message(id: m.id,
                   title: m.title,
                   body: m.body,
                   created: m.created,
                   infoLink: m.infoLink,
                   link: m.link,
                   read: !m.read,
                   subcategory: m.subcategory,
                   tag: m.tag,
                   button: m.button,
                   document: m.document,
                   category: m.category)
}
