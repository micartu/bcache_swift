//
//  MapPatch.swift
//  benzo
//
//  Created by Michael Artuerhof on 17.05.18.
//  Copyright © 2018 MLN Group. All rights reserved.
//

import Foundation

struct MapPatch {
    let id: Int
    let lon1: Double
    let lat1: Double
    let lon2: Double
    let lat2: Double
}

struct MapPatchBase {
    let id: Int
    let date: Date
    let stations: [Station]
}
