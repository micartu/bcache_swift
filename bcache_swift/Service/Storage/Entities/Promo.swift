//
//  Promo.swift
//  benzo
//
//  Created by Michael Artuerhof on 02.10.18.
//  Copyright © 2018 MLN Group. All rights reserved.
//

import Foundation

enum promoType: String {
    case referral = "referral"
    case common = "common"
    case unknown = ""
}

struct Promo {
    let id: String
    let type: promoType
    let dateStart: NSDate
    let dateEnd: NSDate
    let title: String
    let body: String
    let sharing: String
    let link: String
    let rules: String
}
