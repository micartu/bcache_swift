//
//  User.swift
//  benzo
//
//  Created by Michael Artuerhof on 23.04.18.
//  Copyright © 2018 MLN Group. All rights reserved.
//

import Foundation


struct User {
    /// user's login
    var login: String
    /// user's phone
    var phone: String
    /// user's name
    var name: String
    /// user's surname
    var surname: String
    /// user's middlename
    var midname: String
    /// user's avatar
    var avatar: String
    /// user's favorite fuel
    var fuel: String
    /// user's codeword
    var codeword: String
    /// user's card
    var card: String
    /// user card's PIN
    var pin: String
    /// user's email
    var email: String
    /// user's balance
    var balance: Double
    /// user's loyality
    var loyality: UserLoyality
    /// status of the card (blocked or not)
    var cardBlocked: Bool
    /// push notification token
    var apn: String
}

struct UserLoyality {
    // additional stuff
    var cpa_crd_code: String
    var cpa_crd_rub: Int
    var cpa_prt_code: String
    var cpa_prt_name: String
    var cpa_prt_ref1: String
    var cpa_prt_ref2: String
    var cpa_prt_ref3: String
    var cpa_prt_subid: String

    init(cpa_crd_code: String,
         cpa_crd_rub: Int,
         cpa_prt_code: String,
         cpa_prt_name: String,
         cpa_prt_ref1: String,
         cpa_prt_ref2: String,
         cpa_prt_ref3: String,
         cpa_prt_subid: String) {
        self.cpa_crd_code = cpa_crd_code
        self.cpa_crd_rub = cpa_crd_rub
        self.cpa_prt_code = cpa_prt_code
        self.cpa_prt_name = cpa_prt_name
        self.cpa_prt_ref1 = cpa_prt_ref1
        self.cpa_prt_ref2 = cpa_prt_ref2
        self.cpa_prt_ref3 = cpa_prt_ref3
        self.cpa_prt_subid = cpa_prt_subid
    }

    init() {
        self.init(cpa_crd_code: "",
                  cpa_crd_rub: 0,
                  cpa_prt_code: "",
                  cpa_prt_name: "",
                  cpa_prt_ref1: "",
                  cpa_prt_ref2: "",
                  cpa_prt_ref3: "",
                  cpa_prt_subid: "")
    }
}
