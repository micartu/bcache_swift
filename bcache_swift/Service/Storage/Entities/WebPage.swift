//
//  WebPage.swift
//  benzo
//
//  Created by Michael Artuerhof on 29.08.18.
//  Copyright © 2018 MLN Group. All rights reserved.
//

import Foundation

struct WebPage {
    let id: String
    let content: String
}
