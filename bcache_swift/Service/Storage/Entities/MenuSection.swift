//
//  MenuSection.swift
//  benzo
//
//  Created by Michael Artuerhof on 19.02.19.
//  Copyright © 2019 MLN Group. All rights reserved.
//

import Foundation

struct MenuSection {
    var entries: [MenuEntry]
    let order: Int
    let style: MenuStyle
}
