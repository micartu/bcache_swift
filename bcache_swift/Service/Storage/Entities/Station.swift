//
//  Station.swift
//  benzo
//
//  Created by Michael Artuerhof on 27.04.18.
//  Copyright © 2018 MLN Group. All rights reserved.
//

import Foundation

struct Station {
    var id: Int
    var patchId: Int
    var name: String
    var descr: String
    var address: String
    var inCarBuyFuel: Bool
    var prices: String
    let imgSmall: String
    let imgBig: String
    let lon: Double
    let lat: Double
    var countCollumn: Int
}

extension Station: Equatable { }

func ==(lhs: Station, rhs: Station) -> Bool {
    if lhs.lat == rhs.lat && lhs.lon == rhs.lon && lhs.name == rhs.name && lhs.descr == rhs.descr {
        return true
    }
    return false
}
