//
//  Transaction.swift
//  benzo
//
//  Created by Michael Artuerhof on 10.05.18.
//  Copyright © 2018 MLN Group. All rights reserved.
//

import Foundation

struct Transaction {
	var rrn: String
	var date: NSDate
	var trnType: String
	var brandName: String
    var location: String
	var productName: String
	var productQuantity: Double
    var trnBB: Int
	var trnValue: Int
	var points: Int
	var status: String
	var trnComment: String
	var convertBonusDate: NSDate
	var discountSign: Int
	var discountValue: Int
	var discountValueCurrency: Int
	var channel: String
    var innertype: String
}
