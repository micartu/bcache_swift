//
//  CacheTransactionService.swift
//  benzo
//
//  Created by Michael Artuerhof on 30.05.19.
//  Copyright © 2019 MLN Group. All rights reserved.
//

import Foundation

protocol CacheTransactionService {
    func exists(transaction: String) -> Bool
    func getTransaction(rrn: String) -> Transaction?
    func getTransactions(from startDate: NSDate?,
                         page: Int,
                         count: Int) -> [Transaction]
    func getTransactions(of type: String,
                         from startDate: NSDate?,
                         page: Int,
                         count: Int) -> [Transaction]
    func create(transaction t: Transaction, completion: @escaping (() -> Void))
    func create(transactions trs: [Transaction], completion: @escaping (() -> Void))
    func update(transaction: Transaction, completion: @escaping (() -> Void))
    func delete(transaction rrn: String, completion: @escaping (() -> Void))
}
