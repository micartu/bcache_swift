//
//  CacheUserService.swift
//  benzo
//
//  Created by Michael Artuerhof on 28.05.19.
//  Copyright © 2019 MLN Group. All rights reserved.
//

import Foundation

protocol CacheUserService {
    func exists(user: String) -> Bool
    func getCurrentUser() -> User?
    func get(user: String) -> User?
    func create(user: User, completion: @escaping (() -> Void))
    func update(user: User, completion: @escaping (() -> Void))
    func delete(user: String, completion: @escaping (() -> Void))
}
