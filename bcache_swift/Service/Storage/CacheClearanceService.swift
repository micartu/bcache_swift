//
//  CacheClearanceService.swift
//  benzo
//
//  Created by Michael Artuerhof on 06.06.19.
//  Copyright © 2019 MLN Group. All rights reserved.
//

import Foundation

protocol CacheClearanceService {
    func deleteAllCache(completion: @escaping (() -> Void))

    func deleteAllMapPatches(completion: @escaping (() -> Void))
    func deleteAllTransaction(completion: @escaping (() -> Void))
    func deleteAllMessages(completion: @escaping (() -> Void))
    func deleteAllPromos(completion: @escaping (() -> Void))
    func deleteAllWebPages(completion: @escaping (() -> Void))
    func deleteAllLocPoints(completion: @escaping (() -> Void))
    func deleteAllMenuSectionsForCurrentUser(completion: @escaping (() -> Void))
}
