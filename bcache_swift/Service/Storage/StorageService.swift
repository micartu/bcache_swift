//
//  StorageService.swift
//  benzo
//
//  Created by Michael Artuerhof on 23.04.18.
//  Copyright © 2018 MLN Group. All rights reserved.
//

import Foundation

protocol StorageService:
    CacheMiscService,
    CacheMenuService,
    CachePromoService,
    CacheMessageService,
    CacheLocPointService,
    CacheWebPageService,
    CacheTransactionService,
    CacheClearanceService,
    CacheMapPatchService,
    CacheUserService { }
