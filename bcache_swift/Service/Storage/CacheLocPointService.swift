//
//  CacheLocPointService.swift
//  benzo
//
//  Created by Michael Artuerhof on 31.05.19.
//  Copyright © 2019 MLN Group. All rights reserved.
//

import Foundation

protocol CacheLocPointService {
    func getLocPoints() -> [LocPoint]?
    func add(locpoints: [LocPoint], completion: @escaping (() -> Void))
    func add(locpoint p: LocPoint, completion: @escaping (() -> Void))
}
