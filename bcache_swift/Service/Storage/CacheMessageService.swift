//
//  CacheMessageService.swift
//  benzo
//
//  Created by Michael Artuerhof on 31.05.19.
//  Copyright © 2019 MLN Group. All rights reserved.
//

import Foundation

protocol CacheMessageService {
    func getMessagesForCurrentUser(category: String) -> [Message]?
    func getMessagesForCurrentUser(linked l: String, category: String) -> [Message]?
    func update(messages msg: [Message], completion: @escaping (() -> Void))
    func update(message m: Message, completion: @escaping (() -> Void))
    func create(message m: Message, completion: @escaping (() -> Void))
    func create(messages msgs: [Message], completion: @escaping (() -> Void))
}
