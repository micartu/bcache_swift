//
//  Cache.swift
//  benzo
//
//  Created by Michael Artuerhof on 27.05.19.
//  Copyright © 2019 Michael Artuerhof. All rights reserved.
//

import Foundation
import RealmSwift

final class Cache: StorageService {
    init(secrets: SecretService, expirationInterval: TimeInterval) {
        self.secrets = secrets
        self.expirationInterval = expirationInterval
        cqueue = DispatchQueue(label: Const.dqueue + ".cache_completion_queue",
                               qos: .userInitiated)
        rcfg = Realm.Configuration.defaultConfiguration
        realmDates = [String:Date]()
        let defaults = UserDefaults.standard
        stationId = defaults.integer(forKey: const.keyStationId)
    }

    // MARK: - Private / Internal

    internal func getRealm() -> Realm {
        if Thread.isMainThread {
            do {
                return try Realm()
            }
            catch let error {
                fatalError("!!exception in getting realm: \(error)")
            }
        } else {
            return getRealmFromDict()
        }
    }

    private func getRealmFromDict() -> Realm {
        let n = threadId()
        let r: Realm?
        lock.lock()
        // update/add date of realm expiration
        realmDates[n] = Date(timeIntervalSinceNow: const.expireContext)
        if let realm = realms[n] {
            realm.refresh()
            r = realm
        } else {
            r = nil
        }
        lock.unlock()
        if let realm = r {
            return realm
        }
        do {
            let curRealm = try Realm(configuration: rcfg)
            lock.lock()
            realms[n] = curRealm
            lock.unlock()
            DispatchQueue(label: "realm-background").async { [weak self] in
                self?.garbageCollectionForSavedContexts()
            }
            curRealm.refresh()
            return curRealm
        }
        catch let err {
            fatalError("getRealm: cannot return a realm object: \(err) for: '\(n)'")
        }
    }

    internal func garbageCollectionForSavedContexts() {
        var keys = [String]()
        lock.lock()
        for (k, ctx) in realmDates {
            if ctx < Date() {
                keys.append(k)
            }
        }
        for k in keys {
            realmDates.removeValue(forKey: k)
            realms.removeValue(forKey: k)
        }
        lock.unlock()
    }

    internal func modifyIn(realm r: Realm,
                           actions: @escaping (() -> Void),
                           completion:  @escaping (() -> Void)) {
        let g = DispatchGroup()
        g.enter()
        do {
            try r.write {
                actions()
                g.leave()
            }
        }
        catch let error {
            print("!!exception in write: \(error)")
            g.leave()
        }
        g.notify(queue: cqueue) {
            completion()
        }
    }

    internal func saveIds() {
        let defaults = UserDefaults.standard
        lock.lock()
        defaults.set(NSNumber(integerLiteral:Int(stationId)), forKey: const.keyStationId)
        lock.unlock()
        defaults.synchronize()
    }

    deinit {
        saveIds()
    }

    internal let rcfg: Realm.Configuration
    internal var stationId: Int
    internal let expirationInterval: TimeInterval
    internal let lock = NSLock()
    internal var realms = [String:Realm]()
    internal var realmDates: [String:Date]
    internal let secrets: SecretService
    internal let cqueue: DispatchQueue

    internal struct const {
        static let kSeparator: Character = ";"
        static let expireContext: TimeInterval = 5
        static let keyStationId = "key_realm_station_id"
    }
}
