//
//  Cache+Message.swift
//  benzo
//
//  Created by Michael Artuerhof on 31.05.19.
//  Copyright © 2019 MLN Group. All rights reserved.
//

import Foundation
import RealmSwift

extension Cache: CacheMessageService {
    func getMessagesForCurrentUser(category: String) -> [Message]? {
        return getMessagesForCurrentUser(linked: "", category: category)
    }

    func getMessagesForCurrentUser(linked l: String, category: String) -> [Message]? {
        if let u = getIn(realm: getRealm(), login: secrets.user) {
            var format = "(link == %@) "
            if category.count > 0 {
                format += "AND (category == %@)"
            }
            let predicate = NSPredicate(format: format, l, category)
            var out = [Message]()
            let msgs = u.messages.filter(predicate).sorted(byKeyPath: "created", ascending: false)
            for m in msgs {
                let mm = convert(message: m)
                out.append(mm)
                // if we're looking not for messages on the root leve
                // and want to fetch all answers to the messages
                // we've already added then try to attach messages linked to this one
                if l.count > 0 {
                    if let msgsToM = getMessagesForCurrentUser(linked: m.id, category: category) {
                        out.append(contentsOf: msgsToM)
                    }
                }
            }
            return out
        }
        return nil
    }

    func update(messages msg: [Message], completion: @escaping (() -> Void)) {
        let r = getRealm()
        modifyIn(realm: r, actions: { [weak self] in
            if let u = self?.getCurUserIn(realm: r) {
                for m in msg {
                    let predicate = NSPredicate(format: "id == %@", m.id)
                    let rms = u.messages.filter(predicate)
                    for rm in rms {
                        self?.update(message: rm, from: m)
                    }
                }
            }
        }, completion: completion)
    }

    func update(message m: Message, completion: @escaping (() -> Void)) {
        update(messages: [m], completion: completion)
    }

    func create(messages msgs: [Message], completion: @escaping (() -> Void)) {
        let r = getRealm()
        modifyIn(realm: r, actions: { [weak self] in
            if let u = self?.getCurUserIn(realm: r) {
                for m in msgs {
                    let rm = RMessage()

                    rm.id = m.id
                    self?.update(message: rm, from: m)

                    u.messages.append(rm)
                }
            }
        }, completion: completion)
    }

    func create(message m: Message, completion: @escaping (() -> Void)) {
        create(messages: [m], completion: completion)
    }

    // MARK: - Private/Internal

    internal func update(message m: RMessage, from mes: Message) {
        m.title = mes.title
        m.body = mes.body
        m.created = mes.created as Date
        m.document = mes.document
        m.infoLink = mes.infoLink
        m.link = mes.link
        m.button = mes.button
        m.category = mes.category
        m.subcategory = mes.subcategory
        m.tag = mes.tag
        if !m.read { // this flag could be changed only once
            m.read = mes.read
        }
    }

    internal func convert(message m: RMessage) -> Message {
        return Message(id: m.id,
                       title: m.title,
                       body: m.body,
                       created: m.created as NSDate,
                       infoLink: m.infoLink,
                       link: m.link,
                       read: m.read,
                       subcategory: m.subcategory,
                       tag: m.tag,
                       button: m.button,
                       document: m.document,
                       category: m.category)
    }

    internal func deleteAllMessagesIn(realm r: Realm, for u: RUser) {
        r.delete(u.messages)
    }
}
