//
//  Cache+WebPage.swift
//  benzo
//
//  Created by Michael Artuerhof on 31.05.19.
//  Copyright © 2019 MLN Group. All rights reserved.
//

import Foundation
import RealmSwift

extension Cache: CacheWebPageService {
    func get(webpage id: String) -> WebPage? {
        if let pages = getWebPagesIn(realm: getRealm(), id: id) {
            if let rp = pages.first {
                return convert(webpage: rp)
            }
        }
        return nil
    }

    func update(webpage: WebPage, completion: @escaping (() -> Void)) {
        let r = getRealm()
        modifyIn(realm: r, actions: { [weak self] in
            if let pages = self?.getWebPagesIn(realm: r, id: webpage.id) {
                for p in pages {
                    self?.update(rwebpage: p, page: webpage)
                }
            }
        }, completion: completion)
    }

    func create(webpage page: WebPage, completion: @escaping (() -> Void)) {
        let r = getRealm()
        modifyIn(realm: r, actions: { [weak self] in
            self?.createIn(realm: r, webpage: page)
        }, completion: completion)
    }

    func delete(webpage p: WebPage, completion: @escaping (() -> Void)) {
        let r = getRealm()
        modifyIn(realm: r, actions: { [weak self] in
            if let pages = self?.getWebPagesIn(realm: r, id: p.id) {
                for p in pages {
                    r.delete(p)
                }
            }
        }, completion: completion)
    }

    // MARK: - Private/Internal

    @discardableResult
    internal func createIn(realm r: Realm, webpage p: WebPage) -> RWebPage? {
        if let u = getIn(realm: r, login: secrets.user) {
            let rp = RWebPage()
            rp.id = p.id
            update(rwebpage: rp, page: p)
            u.webpages.append(rp)
            return rp
        }
        return nil
    }

    internal func update(rwebpage rp: RWebPage, page p: WebPage) {
        rp.content = p.content
    }

    internal func convert(webpage p: RWebPage) -> WebPage {
        return WebPage(id: p.id,
                       content: p.content)
    }

    private func getWebPagesIn(realm: Realm, id: String) -> Results<RWebPage>? {
        if let u = getIn(realm: realm, login: secrets.user) {
            let predicate = NSPredicate(format: "id == %@", id)
            return { u.webpages.filter(predicate) }()
        }
        return nil
    }

    internal func deleteAllWebpages(realm r: Realm, for u: RUser) {
        r.delete(u.webpages)
    }
}
