//
//  Cache+MapPatch.swift
//  benzo
//
//  Created by Michael Artuerhof on 30.05.19.
//  Copyright © 2019 MLN Group. All rights reserved.
//

import Foundation
import RealmSwift

extension Cache: CacheMapPatchService {
    func exists(patch id: Int) -> Bool {
        if let _ = getIn(realm: getRealm(), patchId: id) {
            return true
        }
        return false
    }

    func isExpired(patch id: Int,
                   expireInterval: TimeInterval,
                   on date: Date) -> Bool {
        if let p = getIn(realm: getRealm(), patchId: id) {
            if let d = p.date as Date? {
                if date > d.addingTimeInterval(expireInterval) {
                    return true
                }
            }
        }
        return false
    }

    func get(patch id: Int) -> MapPatchBase? {
        if let p = getIn(realm: getRealm(), patchId: id) {
            return convert(patch: p)
        }
        return nil
    }

    func getStationsIn(patch id: Int) -> [Station] {
        if let p = getIn(realm: getRealm(), patchId: id) {
            return stationsIn(patch: p)
        }
        return [Station]()
    }

    func create(patch id: Int, completion: @escaping (() -> Void)) {
        let r = getRealm()
        modifyIn(realm: r, actions: { [weak self] in
            self?.createIn(realm: r, patch: id)
        }, completion: completion)
    }

    func delete(patch id: Int, completion: @escaping (() -> Void)) {
        let r = getRealm()
        modifyIn(realm: r, actions: { [weak self] in
            if let rp = self?.getIn(realm: r, patchId: id) {
                let stations = rp.stations
                r.delete(rp)
                r.delete(stations)
            }
        }, completion: completion)
    }

    // MARK: - Private / Internal

    @discardableResult
    internal func createIn(realm r: Realm, patch id: Int) -> RMapPatchBase {
        let p = RMapPatchBase()
        p.id = id
        update(patch: p)
        r.add(p)
        return p
    }

    @discardableResult
    internal func createOrUpdateIn(realm r: Realm, patch id: Int) -> RMapPatchBase {
        if let rp = getIn(realm: r, patchId: id) {
            update(patch: rp)
            return rp
        } else {
            return createIn(realm: r, patch: id)
        }
    }

    internal func update(patch rp: RMapPatchBase) {
        rp.date = Date()
    }

    internal func getIn(realm: Realm, patchId: Int) -> RMapPatchBase? {
        let predicate = NSPredicate(format: "id == %ld", patchId)
        let patches: Results<RMapPatchBase> = { realm.objects(RMapPatchBase.self).filter(predicate) }()
        if patches.count > 0 {
            return patches.first
        }
        return nil
    }

    internal func stationsIn(patch p: RMapPatchBase) -> [Station] {
        var stations = [Station]()
        for cs in p.stations {
            let s = convert(station: cs)
            stations.append(s)
        }
        return stations
    }

    internal func convert(patch p: RMapPatchBase) -> MapPatchBase {
        return MapPatchBase(id: p.id,
                            date: p.date,
                            stations: stationsIn(patch: p))
    }

    internal func deleteAllMapPatchesIn(realm r: Realm) {
        let patches = r.objects(RMapPatchBase.self)
        for p in patches {
            r.delete(p.stations)
        }
        r.delete(patches)
        lock.lock()
        stationId = 1
        lock.unlock()
    }
}
