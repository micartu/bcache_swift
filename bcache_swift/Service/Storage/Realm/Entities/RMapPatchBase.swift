//
//  RMapPatchBase.swift
//  benzo
//
//  Created by Michael Artuerhof on 29.05.19.
//  Copyright © 2019 MLN Group. All rights reserved.
//

import Foundation
import RealmSwift

class RMapPatchBase: Object {
     @objc dynamic var id = 0
     @objc dynamic var date = Date()
     let stations = List<RStation>()
}
