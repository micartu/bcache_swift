//
//  RMenuEntry.swift
//  benzo
//
//  Created by Michael Artuerhof on 03.06.19.
//  Copyright © 2019 MLN Group. All rights reserved.
//

import Foundation
import RealmSwift

class RMenuEntry: Object {
    @objc dynamic var type = 0
    @objc dynamic var order = 0
    @objc dynamic var title = ""
    @objc dynamic var params = ""
    @objc dynamic var style: RMenuStyle!
}
