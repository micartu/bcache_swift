//
//  RMenuStyle.swift
//  benzo
//
//  Created by Michael Artuerhof on 03.06.19.
//  Copyright © 2019 MLN Group. All rights reserved.
//

import Foundation
import RealmSwift

class RMenuStyle: Object {
    @objc dynamic var cellHeight = 0.0
    @objc dynamic var fontSize = 0.0
    @objc dynamic var fontColor = ""
    @objc dynamic var fontWeight = 0
    @objc dynamic var offsets = ""
}
