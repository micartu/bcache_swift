//
//  RMenuSection.swift
//  benzo
//
//  Created by Michael Artuerhof on 03.06.19.
//  Copyright © 2019 MLN Group. All rights reserved.
//

import Foundation
import RealmSwift

class RMenuSection: Object {
    let entries = List<RMenuEntry>()
    @objc dynamic var order = 0
    @objc dynamic var style: RMenuStyle!
}
