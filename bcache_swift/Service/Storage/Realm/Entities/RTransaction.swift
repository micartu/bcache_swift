//
//  RTransaction.swift
//  benzo
//
//  Created by Michael Artuerhof on 30.05.19.
//  Copyright © 2019 MLN Group. All rights reserved.
//

import Foundation
import RealmSwift

class RTransaction: Object {
    @objc dynamic var rrn = ""
    @objc dynamic var date = Date()
    @objc dynamic var trnType = ""
    @objc dynamic var brandName = ""
    @objc dynamic var location = ""
    @objc dynamic var productName = ""
    @objc dynamic var productQuantity = 0.0
    @objc dynamic var trnBB = 0
    @objc dynamic var trnValue = 0
    @objc dynamic var points = 0
    @objc dynamic var status = ""
    @objc dynamic var trnComment = ""
    @objc dynamic var convertBonusDate = Date()
    @objc dynamic var discountSign = 0
    @objc dynamic var discountValue = 0
    @objc dynamic var discountValueCurrency = 0
    @objc dynamic var channel = ""
    @objc dynamic var innertype = ""
}
