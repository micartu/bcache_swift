//
//  RPromo.swift
//  benzo
//
//  Created by Michael Artuerhof on 01.06.19.
//  Copyright © 2019 MLN Group. All rights reserved.
//

import Foundation
import RealmSwift

class RPromo: Object {
    @objc dynamic var id = ""
    @objc dynamic var type = ""
    @objc dynamic var dateStart = Date()
    @objc dynamic var dateEnd = Date()
    @objc dynamic var title = ""
    @objc dynamic var body = ""
    @objc dynamic var sharing = ""
    @objc dynamic var link = ""
    @objc dynamic var rules = ""
}
