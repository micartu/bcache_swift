//
//  RStation.swift
//  benzo
//
//  Created by Michael Artuerhof on 29.05.19.
//  Copyright © 2019 MLN Group. All rights reserved.
//

import Foundation
import RealmSwift

class RStation: Object {
    @objc dynamic var id = 0
    @objc dynamic var patchId = 0
    @objc dynamic var name = ""
    @objc dynamic var descr = ""
    @objc dynamic var address = ""
    @objc dynamic var inCarBuyFuel = false
    @objc dynamic var prices = ""
    @objc dynamic var imgSmall = ""
    @objc dynamic var imgBig = ""
    @objc dynamic var lon = 0.0
    @objc dynamic var lat = 0.0
    @objc dynamic var countCollumn = 0
}
