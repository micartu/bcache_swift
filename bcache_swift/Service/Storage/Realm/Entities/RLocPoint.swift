//
//  RLocPoint.swift
//  benzo
//
//  Created by Michael Artuerhof on 31.05.19.
//  Copyright © 2019 MLN Group. All rights reserved.
//

import Foundation
import RealmSwift

class RLocPoint: Object {
    @objc dynamic var lat = 0.0
    @objc dynamic var lon = 0.0
}
