//
//  RUser.swift
//  benzo
//
//  Created by Michael Artuerhof on 28.05.19.
//  Copyright © 2019 MLN Group. All rights reserved.
//

import Foundation
import RealmSwift

class RUser: Object {
    /// user's login
    @objc dynamic var login = ""
    /// user's phone
    @objc dynamic var phone = ""
    /// user's name
    @objc dynamic var name = ""
    /// user's surname
    @objc dynamic var surname = ""
    /// user's middlename
    @objc dynamic var midname = ""
    /// user's avatar
    @objc dynamic var avatar = ""
    /// user's favorite fuel
    @objc dynamic var fuel = ""
    /// user's codeword
    @objc dynamic var codeword = ""
    /// user's card
    @objc dynamic var card = ""
    /// user card's PIN
    @objc dynamic var pin = ""
    /// user's email
    @objc dynamic var email = ""
    /// user's balance
    @objc dynamic var balance = 0.0
    /// user's loyality
    @objc dynamic var loyality: RUserLoyality!
    /// user's transactions
    let transactions = List<RTransaction>()
    /// user's webpages
    let webpages = List<RWebPage>()
    /// user's locpoints
    let locpoints = List<RLocPoint>()
    /// user's messages
    let messages = List<RMessage>()
    /// user's messages
    let promos = List<RPromo>()
    /// user's menus
    let menuSections = List<RMenuSection>()
    /// menu refreshment date
    @objc dynamic var menuRefreshDate = Date()
    /// status of the card (blocked or not)
    @objc dynamic var cardBlocked = true
}
