//
//  RMessage.swift
//  benzo
//
//  Created by Michael Artuerhof on 31.05.19.
//  Copyright © 2019 MLN Group. All rights reserved.
//

import Foundation
import RealmSwift

class RMessage: Object {
    @objc dynamic var id = ""
    @objc dynamic var title = ""
    @objc dynamic var body = ""
    @objc dynamic var created = Date()
    @objc dynamic var infoLink = ""
    @objc dynamic var link = ""
    @objc dynamic var read = false
    @objc dynamic var subcategory = ""
    @objc dynamic var tag = ""
    @objc dynamic var button = ""
    @objc dynamic var document = ""
    @objc dynamic var category = ""
}
