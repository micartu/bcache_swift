//
//  RUserLoyality.swift
//  benzo
//
//  Created by Michael Artuerhof on 28.05.19.
//  Copyright © 2019 MLN Group. All rights reserved.
//

import Foundation
import RealmSwift

class RUserLoyality: Object {
    @objc dynamic var cpa_crd_code = ""
    @objc dynamic var cpa_crd_rub = 0
    @objc dynamic var cpa_prt_code = ""
    @objc dynamic var cpa_prt_name = ""
    @objc dynamic var cpa_prt_ref1 = ""
    @objc dynamic var cpa_prt_ref2 = ""
    @objc dynamic var cpa_prt_ref3 = ""
    @objc dynamic var cpa_prt_subid = ""
}
