//
//  RWebPage.swift
//  benzo
//
//  Created by Michael Artuerhof on 31.05.19.
//  Copyright © 2019 MLN Group. All rights reserved.
//

import Foundation
import RealmSwift

class RWebPage: Object {
    @objc dynamic var id = ""
    @objc dynamic var content = ""
}
