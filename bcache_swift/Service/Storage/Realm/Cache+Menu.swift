//
//  Cache+Menu.swift
//  benzo
//
//  Created by Michael Artuerhof on 03.06.19.
//  Copyright © 2019 MLN Group. All rights reserved.
//

import Foundation
import RealmSwift

extension Cache: CacheMenuService {
    func getStoredMenuSectionsForCurrentUser() -> [MenuSection]? {
        if let u = getCurUserIn(realm: getRealm()) {
            var out = [MenuSection]()
            let sections = u.menuSections.sorted(byKeyPath: "order", ascending: true)
            for rs in sections {
                let s = convert(menuSection: rs)
                out.append(s)
            }
            return out
        }
        return nil
    }

    func storedMenuSectionsForCurrentUserExpired() -> Bool {
        if let u = getCurUserIn(realm: getRealm()) {
            if u.menuSections.count > 0 {
                let date = Date() // current date
                if date < u.menuRefreshDate.addingTimeInterval(expirationInterval) {
                    return false
                }
            }
        }
        return true
    }

    func storeForCurrentUser(menuSections ms: [MenuSection],
                             completion: @escaping (() -> Void)) {
        let r = getRealm()
        modifyIn(realm: r, actions: { [weak self] in
            guard let `self` = self else { return }
            if let u = self.getCurUserIn(realm: r) {
                u.menuRefreshDate = Date()
                u.menuSections.removeAll()
                var order = 1
                var lastUsedOrders = [Int]()
                for s in ms {
                    let cmsection = RMenuSection()
                    if s.order != 0 {
                        order = s.order
                    } else {
                        lastUsedOrders.append(order)
                    }
                    order = self.check(lastUsed: &lastUsedOrders, order: order)
                    cmsection.order = order
                    for m in s.entries {
                        let cm = RMenuEntry()
                        self.update(menuentry: cm, from: m)
                        if m.order == 0 {
                            cm.order = order
                            order += 1
                        }
                        let cs: RMenuStyle?
                        if let s = m.style {
                            cs = self.createIn(menuStyle: s)
                            r.add(cs!)
                        } else {
                            cs = nil
                        }
                        cm.style = cs
                        cmsection.entries.append(cm)
                    }
                    cmsection.style = self.createIn(menuStyle: s.style)
                    u.menuSections.append(cmsection)
                    order += 1
                }
            }
        }, completion: completion)
    }

    // MARK: - Private/Internal

    @discardableResult
    internal func createIn(menuStyle s: MenuStyle) -> RMenuStyle {
        let cs = RMenuStyle()
        update(style: cs, from: s)
        return cs
    }

    private func check(lastUsed: inout [Int], order: Int, append: Bool = false) -> Int {
        if lastUsed.contains(order) {
            return check(lastUsed: &lastUsed, order: order + 1, append: true)
        } else {
            if append {
                lastUsed.append(order)
            }
            return order
        }
    }

    internal func update(menuentry cm: RMenuEntry, from m: MenuEntry) {
        cm.type = m.type.rawValue
        cm.title = m.title
        cm.order = m.order
        cm.params = m.params
        // style must be updated somewhere else
    }

    internal func update(style cs: RMenuStyle, from s: MenuStyle) {
        cs.cellHeight = s.cellHeight
        cs.fontSize = s.fontSize
        if let c = s.fontColor {
            let (r,g,b,a) = c.rgbComponents
            let (ri,gi,bi,ai) = (Int(r*255), Int(g*255), Int(b*255), Int(a*255))
            cs.fontColor = String(format:"#%02X%02X%02X%02X", ri, gi, bi, ai)
        } else {
            cs.fontColor = ""
        }
        cs.fontWeight = s.fontWeight.rawValue
        cs.offsets = "\(s.soffsets.top)\(const.kSeparator)\(s.soffsets.bottom)"
    }

    internal func convert(menuSection m: RMenuSection) -> MenuSection {
        var menus = [MenuEntry]()
        let smm = m.entries.sorted(byKeyPath: "order", ascending: true)
        for rm in smm {
            let m = convert(menuEntry: rm)
            menus.append(m)
        }
        let style: MenuStyle
        if let cs = m.style {
            style = convert(menuStyle: cs)
        } else {
            style = styleOf(fontSize: 12)
        }
        return MenuSection(entries: menus,
                           order: m.order,
                           style: style)
    }

    internal func convert(menuEntry m: RMenuEntry) -> MenuEntry {
        let type: menuType
        if let t = menuType(rawValue: Int(m.type)) {
            type = t
        } else {
            type = .offer
        }
        let style: MenuStyle?
        if let cs = m.style {
            style = convert(menuStyle: cs)
        } else {
            style = nil
        }
        return MenuEntry(type: type,
                         order: Int(m.order),
                         title: m.title,
                         params: m.params,
                         style: style)
    }

    internal func convert(menuStyle s: RMenuStyle) -> MenuStyle {
        let top: Double
        let bottom: Double
        let offsets = s.offsets.split(separator: const.kSeparator).map({String($0)})
        if offsets.count > 1 {
            top = offsets[0].toDouble
            bottom = offsets[1].toDouble
        } else {
            top = 0
            bottom = 0
        }
        let weight: menuFontWeight
        if let w = menuFontWeight(rawValue: Int(s.fontWeight)) {
            weight = w
        } else {
            weight = .normal
        }
        let color: UIColor? = UIColor(hexString: s.fontColor)
        return MenuStyle(cellHeight: s.cellHeight,
                         fontSize: s.fontSize,
                         fontColor: color,
                         fontWeight: weight,
                         soffsets: menuSectionOffset(top: top,
                                                     bottom: bottom))
    }
}
