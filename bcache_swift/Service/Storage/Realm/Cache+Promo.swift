//
//  Cache+Promo.swift
//  benzo
//
//  Created by Michael Artuerhof on 31.05.19.
//  Copyright © 2019 MLN Group. All rights reserved.
//

import Foundation
import RealmSwift

extension Cache: CachePromoService {
    func exists(promo: String) -> Bool {
        let r = getRealm()
        if let u = getCurUserIn(realm: r) {
            let predicate = NSPredicate(format: "id == %@", promo)
            if u.promos.filter(predicate).count > 0 {
                return true
            }
        }
        return false
    }

    func getPromosForCurrentUser() -> [Promo]? {
        if let u = getCurUserIn(realm: getRealm()) {
            let promos = u.promos.sorted(byKeyPath: "dateEnd", ascending: false)
            var out = [Promo]()
            for rp in promos {
                let p = convert(promo: rp)
                out.append(p)
            }
            return out
        }
        return nil
    }

    func update(promos prms: [Promo], completion: @escaping (() -> Void)) {
        let r = getRealm()
        modifyIn(realm: r, actions: { [weak self] in
            if let u = self?.getCurUserIn(realm: r) {
                for p in prms {
                    let predicate = NSPredicate(format: "id == %@", p.id)
                    let rpr = u.promos.filter(predicate)
                    for rp in rpr {
                        self?.update(promo: rp, from: p)
                    }
                }
            }
        }, completion: completion)
    }

    // MARK: - Private/Internal

    internal func update(promo p: RPromo, from pr: Promo) {
        p.dateStart = pr.dateStart as Date
        p.dateEnd = pr.dateEnd as Date
        p.title = pr.title
        p.type = pr.type.rawValue
        p.body = pr.body
        p.sharing = pr.sharing
        p.link = pr.link
        p.rules = pr.rules
    }

    internal func convert(promo p: RPromo) -> Promo {
        let type: promoType
        if let t = promoType(rawValue: p.type) {
            type = t
        } else {
            type = .unknown
        }
        return Promo(id: p.id,
                     type: type,
                     dateStart: p.dateStart as NSDate,
                     dateEnd: p.dateEnd as NSDate,
                     title: p.title,
                     body: p.body,
                     sharing: p.sharing,
                     link: p.link,
                     rules: p.rules)
    }

    internal func deleteAllPromosIn(realm r: Realm, for u: RUser) {
        r.delete(u.promos)
    }
}
