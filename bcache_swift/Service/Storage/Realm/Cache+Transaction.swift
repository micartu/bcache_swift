//
//  Cache+Transaction.swift
//  benzo
//
//  Created by Michael Artuerhof on 30.05.19.
//  Copyright © 2019 MLN Group. All rights reserved.
//

import Foundation
import RealmSwift

extension Cache: CacheTransactionService {
    func exists(transaction: String) -> Bool {
        let r = getRealm()
        if let rts = getTransactionsIn(realm: r, rrn: transaction) {
            if rts.count > 0 {
                return true
            }
        }
        return false
    }

    func getTransaction(rrn: String) -> Transaction? {
        let r = getRealm()
        if let rts = getTransactionsIn(realm: r, rrn: rrn) {
            if rts.count > 0 {
                if let rt = rts.first {
                    return convert(transaction: rt)
                }
            }
        }
        return nil
    }

    func getTransactions(from startDate: NSDate?,
                         page: Int,
                         count: Int) -> [Transaction] {
        return getTransactions(stringFormat: "",
                               args: nil,
                               from: startDate,
                               page: page,
                               count: count)
    }

    func getTransactions(of type: String,
                         from startDate: NSDate?,
                         page: Int,
                         count: Int) -> [Transaction] {
        return getTransactions(stringFormat: "(innertype == %@)",
                               args: [type],
                               from: startDate,
                               page: page,
                               count:count)
    }

    func create(transaction t: Transaction, completion: @escaping (() -> Void)) {
        create(transactions: [t], completion: completion)
    }

    func create(transactions trs: [Transaction], completion: @escaping (() -> Void)) {
        let r = getRealm()
        if let u = getIn(realm: r, login: secrets.user) {
            modifyIn(realm: r, actions: { [weak self] in
                for t in trs {
                    let rt = RTransaction()
                    rt.rrn = t.rrn
                    self?.updateIn(transaction: rt, from: t)
                    //r.add(rt)
                    u.transactions.append(rt)
                }
            }, completion: completion)
        } else {
            // no user, no creation of transactions
            completion()
        }
    }

    func update(transaction: Transaction, completion: @escaping (() -> Void)) {
        let r = getRealm()
        if let rts = getTransactionsIn(realm: r, rrn: transaction.rrn) {
            if rts.count > 0 {
                if let rt = rts.first {
                    updateIn(transaction: rt, from: transaction)
                }
            }
        }
        completion()
    }

    func delete(transaction rrn: String, completion: @escaping (() -> Void)) {
        let r = getRealm()
        modifyIn(realm: r, actions: { [weak self] in
            if let rts = self?.getTransactionsIn(realm: r, rrn: rrn) {
                for rt in rts {
                    r.delete(rt)
                }
            }
        }, completion: completion)
    }

    // MARK: - Private/Internal

    private func getTransactionsIn(realm: Realm, rrn: String) -> Results<RTransaction>? {
        if let u = getIn(realm: realm, login: secrets.user) {
            let predicate = NSPredicate(format: "rrn == %@", rrn)
            return { u.transactions.filter(predicate) }()
        }
        return nil
    }

    private func getTransactions(stringFormat: String,
                                 args: [Any]?,
                                 from startDate: NSDate?,
                                 page: Int,
                                 count: Int) -> [Transaction] {
        var format = ""
        if stringFormat.count > 0 {
            format += stringFormat + " AND "
        }
        let start: NSDate
        if let sd = startDate {
            format += "(date >= %@)"
            start = sd
        } else {
            start = NSDate()
        }
        let argToPass: [Any]
        if let a = args {
            argToPass = a + [start]
        } else {
            if format.count > 0 {
                argToPass = [start]
            } else {
                argToPass = []
            }
        }
        let predicate: NSPredicate? = format.count > 0 ?
            NSPredicate(format: format, argumentArray: argToPass) : nil
        return getTransactions(with: predicate, page: page, count: count)
    }

    private func getTransactions(with predicate: NSPredicate?,
                                 page: Int,
                                 count: Int) -> [Transaction] {
        let r = getRealm()
        var out = [Transaction]()
        if let ru = getIn(realm: r, login: secrets.user) {
            let trans = ru.transactions
            let rtrans: Results<RTransaction>
            if let p = predicate {
                rtrans = trans.filter(p).sorted(byKeyPath: "date", ascending: false)
            } else {
                rtrans = trans.sorted(byKeyPath: "date", ascending: false)
            }
            let prange = (page * count)..<((page + 1) * count)
            let elems = rtrans.startIndex..<rtrans.endIndex
            let offset = mergeRange(from: elems, with: prange)
            for rt in rtrans[offset] {
                let t = convert(transaction: rt)
                out.append(t)
            }
        }
        return out
    }

    internal func convert(transaction t: RTransaction) -> Transaction {
        return Transaction(rrn: t.rrn,
                           date: t.date as NSDate,
                           trnType: t.trnType,
                           brandName: t.brandName,
                           location: t.location,
                           productName: t.productName,
                           productQuantity: t.productQuantity,
                           trnBB: t.trnBB,
                           trnValue: t.trnValue,
                           points: t.points,
                           status: t.status,
                           trnComment: t.trnComment,
                           convertBonusDate: t.convertBonusDate as NSDate,
                           discountSign: t.discountSign,
                           discountValue: t.discountValue,
                           discountValueCurrency: t.discountValueCurrency,
                           channel: t.channel,
                           innertype: t.innertype)
    }

    internal func updateIn(transaction ct: RTransaction, from t: Transaction) {
        ct.date = t.date as Date
        ct.trnType = t.trnType
        ct.innertype = t.innertype
        ct.brandName = t.brandName
        ct.location = t.location
        ct.productName = t.productName
        ct.productQuantity = t.productQuantity
        ct.trnValue = t.trnValue
        ct.points = t.points
        ct.trnBB = t.trnBB
        ct.status = t.status
        ct.trnComment = t.trnComment
        ct.convertBonusDate = t.convertBonusDate as Date
        ct.discountSign = t.discountSign
        ct.discountValue = t.discountValue
        ct.discountValueCurrency = t.discountValueCurrency
        ct.channel = t.channel
    }

    internal func deleteAllTransactionsIn(realm r: Realm, for u: RUser) {
        r.delete(u.transactions)
    }
}
