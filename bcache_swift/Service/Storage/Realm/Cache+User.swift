//
//  Cache+User.swift
//  benzo
//
//  Created by Michael Artuerhof on 30.05.19.
//  Copyright © 2019 MLN Group. All rights reserved.
//

import Foundation
import RealmSwift

extension Cache: CacheUserService {
    func exists(user: String) -> Bool {
        let predicate = NSPredicate(format: "login == %@", user)
        let users: Results<RUser> = { getRealm().objects(RUser.self).filter(predicate) }()
        return users.count > 0
    }

    func getCurrentUser() -> User? {
        return get(user: secrets.user)
    }

    func get(user: String) -> User? {
        if let ru = getIn(realm: getRealm(), login: user) {
            return convert(user: ru)
        }
        return nil
    }

    func create(user: User, completion: @escaping (() -> Void)) {
        let r = getRealm()
        modifyIn(realm: r, actions: { [weak self] in
            let ru = RUser()
            ru.loyality = RUserLoyality()
            self?.updateIn(ruser: ru, user: user)
            r.add(ru)
        }, completion: completion)
    }

    func update(user: User, completion: @escaping (() -> Void)) {
        let r = getRealm()
        if let ru = getIn(realm: r, login: user.login) {
            modifyIn(realm: r, actions: { [weak self] in
                self?.updateIn(ruser: ru, user: user)
            }, completion: completion)
        }
    }

    func delete(user: String, completion: @escaping (() -> Void)) {
        let r = getRealm()
        modifyIn(realm: r, actions: { [weak self] in
            guard let `self` = self else { return }
            let users: Results<RUser> = self.getResultsIn(realm: r, login: user)
            for ru in users {
                self.deleteAllPromosIn(realm: r, for: ru)
                self.deleteAllMessagesIn(realm: r, for: ru)
                self.deleteAllLocPointsIn(realm: r, for: ru)
                self.deleteAllWebpages(realm: r, for: ru)
                self.deleteAllTransactionsIn(realm: r, for: ru)
                r.delete(ru.loyality)
                r.delete(ru)
            }
        }, completion: completion)
    }

    // MARK: - Private / internal
    private func getResultsIn(realm: Realm, login: String) -> Results<RUser> {
        let predicate = NSPredicate(format: "login == %@", login)
        return { realm.objects(RUser.self).filter(predicate) }()
    }

    internal func getIn(realm: Realm, login: String) -> RUser? {
        let users: Results<RUser> = getResultsIn(realm: realm, login: login)
        if users.count > 0 {
            return users.first
        }
        return nil
    }

    internal func getCurUserIn(realm: Realm) -> RUser? {
        return getIn(realm: realm, login: secrets.user)
    }

    internal func convert(user u: RUser) -> User {
        let card = u.card
        let dcard = secrets.decrypt(card)
        let l = u.loyality!
        let loyality = UserLoyality(cpa_crd_code: l.cpa_crd_code,
                                    cpa_crd_rub: l.cpa_crd_rub,
                                    cpa_prt_code: l.cpa_prt_code,
                                    cpa_prt_name: l.cpa_prt_name,
                                    cpa_prt_ref1: l.cpa_prt_ref1,
                                    cpa_prt_ref2: l.cpa_prt_ref2,
                                    cpa_prt_ref3: l.cpa_prt_ref3,
                                    cpa_prt_subid: l.cpa_prt_subid)
        let spin: String
        let sep = u.pin.split(separator: "|")
        if sep.count > 1 {
            spin = u.pin
        } else if !u.pin.contains("|") {
            spin = "0|\(u.pin)"
        } else {
            spin = u.pin
        }
        return User(login: u.login,
                    phone: u.phone,
                    name: u.name,
                    surname: u.surname,
                    midname: u.midname,
                    avatar: u.avatar,
                    fuel: u.fuel,
                    codeword: u.codeword,
                    card: dcard,
                    pin: spin,
                    email: u.email,
                    balance: u.balance,
                    loyality: loyality,
                    cardBlocked: u.cardBlocked,
                    apn: "")
    }

    private func updateIn(ruser cu: RUser, user u: User) {
        cu.balance = u.balance
        cu.email = u.email
        cu.surname = u.surname
        cu.midname = u.midname
        cu.card = secrets.encrypt(u.card)
        cu.name = u.name
        cu.login = u.login
        cu.phone = u.phone
        cu.codeword = u.codeword
        cu.fuel = u.fuel
        cu.cardBlocked = u.cardBlocked
        cu.pin = u.pin
        cu.avatar = u.avatar

        // loyality
        cu.loyality.cpa_crd_code = u.loyality.cpa_crd_code
        cu.loyality.cpa_crd_rub = u.loyality.cpa_crd_rub
        cu.loyality.cpa_prt_code = u.loyality.cpa_prt_code
        cu.loyality.cpa_prt_name = u.loyality.cpa_prt_name
        cu.loyality.cpa_prt_ref1 = u.loyality.cpa_prt_ref1
        cu.loyality.cpa_prt_ref2 = u.loyality.cpa_prt_ref2
        cu.loyality.cpa_prt_ref3 = u.loyality.cpa_prt_ref3
        cu.loyality.cpa_prt_subid = u.loyality.cpa_prt_subid
    }
}
