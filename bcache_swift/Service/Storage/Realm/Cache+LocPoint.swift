//
//  Cache+LocPoint.swift
//  benzo
//
//  Created by Michael Artuerhof on 31.05.19.
//  Copyright © 2019 MLN Group. All rights reserved.
//

import Foundation
import RealmSwift

extension Cache: CacheLocPointService {
    func getLocPoints() -> [LocPoint]? {
        if let u = getIn(realm: getRealm(), login: secrets.user) {
            var out = [LocPoint]()
            for rp in u.locpoints {
                let p = convert(locpoint: rp)
                out.append(p)
            }
            return out
        }
        return nil
    }

    func add(locpoints: [LocPoint], completion: @escaping (() -> Void)) {
        let r = getRealm()
        modifyIn(realm: r, actions: { [weak self] in
            if let u = self?.getCurUserIn(realm: r) {
                for l in locpoints {
                    let rl = RLocPoint()

                    rl.lat = l.lat
                    rl.lon = l.lon

                    u.locpoints.append(rl)
                }
            }
        }, completion: completion)
    }

    func add(locpoint p: LocPoint, completion: @escaping (() -> Void)) {
        add(locpoints: [p], completion: completion)
    }

    // MARK: - Private/Internal

    internal func convert(locpoint p: RLocPoint) -> LocPoint {
        return LocPoint(lat: p.lat, lon: p.lon)
    }

    internal func deleteAllLocPointsIn(realm r: Realm, for u: RUser) {
        r.delete(u.locpoints)
    }
}
