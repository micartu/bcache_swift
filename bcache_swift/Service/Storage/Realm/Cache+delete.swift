//
//  Cache+delete.swift
//  benzo
//
//  Created by Michael Artuerhof on 06.06.19.
//  Copyright © 2019 MLN Group. All rights reserved.
//

import Foundation
import RealmSwift

extension Cache: CacheClearanceService {
    func deleteAllCache(completion: @escaping (() -> Void)) {
        let r = getRealm()
        modifyIn(realm: r, actions: { [weak self] in
            if let u = self?.getCurUserIn(realm: r) {
                self?.deleteAllTransactionsIn(realm: r, for: u)
                self?.deleteAllLocPointsIn(realm: r, for: u)
                self?.deleteAllWebpages(realm: r, for: u)
                self?.deleteAllMessagesIn(realm: r, for: u)
                self?.deleteAllPromosIn(realm: r, for: u)
                self?.deleteAllMenuSectionsIn(realm: r, for: u)
            }
            self?.deleteAllMapPatchesIn(realm: r)
        }, completion: completion)
    }

    func deleteAllMapPatches(completion: @escaping (() -> Void)) {
        let r = getRealm()
        modifyIn(realm: r, actions: { [weak self] in
            self?.deleteAllMapPatchesIn(realm: r)
        }, completion: completion)
    }

    func deleteAllTransaction(completion: @escaping (() -> Void)) {
        let r = getRealm()
        modifyIn(realm: r, actions: { [weak self] in
            if let u = self?.getCurUserIn(realm: r) {
                self?.deleteAllTransactionsIn(realm: r, for: u)
            }
        }, completion: completion)
    }

    func deleteAllWebPages(completion: @escaping (() -> Void)) {
        let r = getRealm()
        modifyIn(realm: r, actions: { [weak self] in
            if let u = self?.getCurUserIn(realm: r) {
                self?.deleteAllWebpages(realm: r, for: u)
            }
        }, completion: completion)
    }

    func deleteAllLocPoints(completion: @escaping (() -> Void)) {
        let r = getRealm()
        modifyIn(realm: r, actions: { [weak self] in
            if let u = self?.getCurUserIn(realm: r) {
                self?.deleteAllLocPointsIn(realm: r, for: u)
            }
        }, completion: completion)
    }

    func deleteAllMessages(completion: @escaping (() -> Void)) {
        let r = getRealm()
        modifyIn(realm: r, actions: { [weak self] in
            if let u = self?.getCurUserIn(realm: r) {
                self?.deleteAllMessagesIn(realm: r, for: u)
            }
        }, completion: completion)
    }

    func deleteAllPromos(completion: @escaping (() -> Void)) {
        let r = getRealm()
        modifyIn(realm: r, actions: { [weak self] in
            if let u = self?.getCurUserIn(realm: r) {
                self?.deleteAllPromosIn(realm: r, for: u)
            }
        }, completion: completion)
    }

    func deleteAllMenuSectionsForCurrentUser(completion: @escaping (() -> Void)) {
        let r = getRealm()
        modifyIn(realm: r, actions: { [weak self] in
            if let u = self?.getCurUserIn(realm: r) {
                self?.deleteAllMenuSectionsIn(realm: r, for: u)
            }
        }, completion: completion)
    }

    // MARK: - Private/Internal

    func deleteAllMenuSectionsIn(realm r: Realm, for u: RUser) {
        for s in u.menuSections {
            let mm = s.entries
            for m in mm {
                if let s = m.style {
                    r.delete(s)
                }
            }
            r.delete(s.entries)
            r.delete(s.style)
        }
        r.delete(u.menuSections)
    }
}
