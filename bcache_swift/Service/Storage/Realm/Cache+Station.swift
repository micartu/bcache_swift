//
//  Cache+Station.swift
//  benzo
//
//  Created by Michael Artuerhof on 29.05.19.
//  Copyright © 2019 MLN Group. All rights reserved.
//

import Foundation
import RealmSwift

extension Cache: CacheStationService {
    func get(station id: Int) -> Station? {
        let r = getRealm()
        if let rs = getIn(realm: r, stationId: id) {
            return convert(station: rs)
        }
        return nil
    }

    func getStationsIn(lon1: Double,
                       lat1: Double,
                       lon2: Double,
                       lat2: Double) -> [Station] {
        let predicate = NSPredicate(format: "(lon >= %g) AND (lon <= %g) AND (lat >= %g) AND (lat <= %g)",
                                    lon1, lon2, lat2, lat1)
        return getStations(with: predicate, realm: getRealm())
    }

    func create(station: Station, completion: @escaping ((Int) -> Void)) {
        create(stations: [station]) { ids in
            let id: Int
            if let _id = ids.first {
                id = _id
            } else {
                id = 0
            }
            completion(id)
        }
    }

    func create(stations sts: [Station], completion: @escaping (([Int]) -> Void)) {
        var ids = [Int]()
        let r = getRealm()
        modifyIn(realm: r, actions: { [weak self] in
            guard let `self` = self else { return }
            for s in sts {
                let rs = RStation()
                self.lock.lock()
                rs.id = self.stationId
                self.stationId += 1
                self.lock.unlock()
                self.updateIn(rstation: rs, station: s)
                ids.append(rs.id)
                r.add(rs)
                let rp = self.createOrUpdateIn(realm: r, patch: s.patchId)
                rp.stations.append(rs)
            }
        }, completion: {
            completion(ids)
        })
    }

    func update(station: Station, completion: @escaping (() -> Void)) {
        let r = getRealm()
        if let s = getIn(realm: r, stationId: station.id) {
            modifyIn(realm: r, actions: { [weak self] in
                self?.updateIn(rstation: s, station: station)
            }, completion: completion)
        } else {
            completion()
        }
    }

    func delete(station s: Station, completion: @escaping (() -> Void)) {
        delete(stations: [s], completion: completion)
    }

    func delete(stations sts: [Station], completion: @escaping (() -> Void)) {
        let r = getRealm()
        modifyIn(realm: r, actions: { [weak self] in
            guard let `self` = self else { return }
            for s in sts {
                for rs in self.getStationsIn(realm: r, stationId: s.id) {
                    if let p = self.getIn(realm: r, patchId: s.patchId) {
                        if let ind = p.stations.index(of: rs) {
                            p.stations.remove(at: ind)
                        }
                        if p.stations.count == 0 {
                            r.delete(p)
                        }
                    }
                    r.delete(rs)
                }
            }
        }, completion: completion)
    }

    // MARK: - Private / internal

    private func updateIn(rstation cs: RStation, station s: Station) {
        cs.countCollumn = s.countCollumn
        cs.descr = s.descr
        cs.lon = s.lon
        cs.lat = s.lat
        cs.name = s.name
        cs.patchId = s.patchId
        cs.inCarBuyFuel = s.inCarBuyFuel
        cs.address = s.address
        cs.imgSmall = s.imgSmall
        cs.imgBig = s.imgBig
    }

    private func getStationsIn(realm: Realm, stationId: Int) -> Results<RStation> {
        let predicate = NSPredicate(format: "id == %ld", stationId)
        return { realm.objects(RStation.self).filter(predicate) }()
    }

    private func getIn(realm: Realm, stationId: Int) -> RStation? {
        let stations: Results<RStation> = getStationsIn(realm: realm, stationId: stationId)
        if stations.count > 0 {
            return stations.first
        }
        return nil
    }

    internal func getStations(with predicate: NSPredicate,
                              realm: Realm) -> [Station] {
        var out = [Station]()
        let stations: Results<RStation> = { realm.objects(RStation.self).filter(predicate) }()
        for s in stations {
            let station = convert(station: s)
            out.append(station)
        }
        return out
    }

    internal func convert(station s: RStation) -> Station {
        let pid = getPatchId(lon: s.lon, lat: s.lat)
        return Station(id: s.id,
                       patchId: pid,
                       name: s.name,
                       descr: s.descr,
                       address: s.address,
                       inCarBuyFuel: s.inCarBuyFuel,
                       prices: "",
                       imgSmall: s.imgSmall,
                       imgBig: s.imgBig,
                       lon: s.lon,
                       lat: s.lat,
                       countCollumn: s.countCollumn)
    }
}
