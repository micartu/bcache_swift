//
//  CacheWebPageService.swift
//  benzo
//
//  Created by Michael Artuerhof on 31.05.19.
//  Copyright © 2019 MLN Group. All rights reserved.
//

import Foundation

protocol CacheWebPageService {
    func get(webpage id: String) -> WebPage?
    func update(webpage: WebPage, completion: @escaping (() -> Void))
    func create(webpage page: WebPage, completion: @escaping (() -> Void))
    func delete(webpage p: WebPage, completion: @escaping (() -> Void))
}
