//
//  CachePromoService.swift
//  benzo
//
//  Created by Michael Artuerhof on 31.05.19.
//  Copyright © 2019 MLN Group. All rights reserved.
//

import Foundation

protocol CachePromoService {
    func exists(promo: String) -> Bool
    func getPromosForCurrentUser() -> [Promo]?
    func update(promos prms: [Promo], completion: @escaping (() -> Void))
}
