//
//  defaultMenus.swift
//  benzo
//
//  Created by Michael Artuerhof on 19.02.19.
//  Copyright © 2019 MLN Group. All rights reserved.
//

import Foundation

func menuOf(type: menuType, title: String) -> MenuEntry {
    return MenuEntry(type: type,
                     order: 0,
                     title: title,
                     params: "",
                     style: nil)
}

func styleOf(fontSize: Double,
                     weight: menuFontWeight = .medium,
                     top: Double = 0,
                     bottom: Double = 20) -> MenuStyle {
    return MenuStyle(cellHeight: 42,
                     fontSize: fontSize,
                     fontColor: nil,
                     fontWeight: weight,
                     soffsets: menuSectionOffset(top: top,
                                                 bottom: bottom))
}

func sectionOf(entries: [MenuEntry], style: MenuStyle) -> MenuSection {
    return MenuSection(entries: entries,
                       order: 0,
                       style: style)
}
