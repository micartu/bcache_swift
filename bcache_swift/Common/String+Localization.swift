//
//  String+Localization.swift
//  benzo
//
//  Created by Michael Artuerhof on 15.06.18.
//  Copyright © 2018 MLN Group. All rights reserved.
//

import Foundation

extension String {
    var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
    }
}
