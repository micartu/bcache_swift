//
//  functions.swift
//  benzo
//
//  Created by Michael Artuerhof on 25.07.18.
//  Copyright © 2018 MLN Group. All rights reserved.
//

import Foundation

func delay(_ delay: Double, closure: @escaping ()->()) {
    let when = DispatchTime.now() + delay
    DispatchQueue.main.asyncAfter(deadline: when, execute: closure)
}

func runOnMainThread(_ closure: @escaping ()->()) {
    if Thread.isMainThread {
        closure()
    } else {
        DispatchQueue.main.async {
            closure()
        }
    }
}

func threadId() -> String {
    return Thread.current.debugDescription
}

func mergeRange(from: Range<Int>, with: Range<Int>) -> Range<Int> {
    let lower: Int
    if with.lowerBound <= from.upperBound {
        if with.lowerBound >= from.lowerBound {
            lower = with.lowerBound
        } else {
            lower = from.lowerBound
        }
    } else {
        lower = from.lowerBound
    }
    let upper = with.upperBound <= from.upperBound ?
        with.upperBound : from.upperBound
    return lower..<upper
}

func getPatchId(lon: Double, lat: Double) -> Int {
    let a = (lon >= 0) ? 1 : 0
    let b = ((lat > 0) ? 1 : 0) << 1
    let llon = Int(abs(lon) / Const.dLon) << 3
    let llat = Int(abs(lat) / Const.dLat) << 15
    
    return a | b | llon | llat
}
