//
//  const_unique.swift
//  benzo
//
//  Created by Michael Artuerhof on 04.02.19.
//  Copyright © 2019 MLN Group. All rights reserved.
//

import Foundation

extension Const {
    // string constants
    static let dqueue = "com.mlngroup.benzo"
    static let iv = "vsBUDKYc"
    static let domain = "benzo_domain"
}
