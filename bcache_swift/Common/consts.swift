//
//  consts.swift
//  benzo
//
//  Created by Michael Artuerhof on 28.02.18.
//  Copyright © 2018 MLN Group. All rights reserved.
//

import Foundation

struct Const {
    static let os: String = "ios"

    // error codes
    static let kErrNet = -1
    static let kErrNetAuth = -2
    static let kErrCards = -3

    // intervals
    static let kAnimationTime: TimeInterval = 0.6
    static let kDefaultExpiration: TimeInterval = 60 * 60 * 24 * 7
    static let kPOIExpiration: TimeInterval = 60 * 60 * 24

    // deltas for map patches generation
    static let dLon = 0.0710678041
    static let dLat = 0.0713678069

    struct msgs {
        static let me = "me"
    }

    struct keys {
        static let poiExp: String = "benzo_POI_EXPIRATION_DATE"
        static let locations: String = "benzo_location_storage"
        static let imgExpiration: String = "benzo_SYSVAL_IMG_EXPIRATION_INTERVAL"
        static let patchExpiration: String = "benzo_SYSVAL_PATCH_EXPIRATION_INTERVAL"
        static let favFuelsTypes: String = "benzo_key_fav_fuels"
        static let adFunc: String = "benzo_key_ad_guy_functions"
        static let favFuel: String = "benzo_key_fav_fuel"
        static let push2act: String = "benzo_key_push_for_action"
        static let shownOnBoarding: String = "benzo_key_shownOnBoarding"
        static let avatarPath: String = "benzo_key_avatar_path"
        static let avatarHash: String = "benzo_key_avatar_hash"
        static let curLat: String = "benzo_key_cur_latitude"
        static let curLon: String = "benzo_key_cur_longitude"
        static let lastTaskMarker = "benzo_key_last_task_date"
        static let notifMoveToMenu: String = "moveToMenu"
        static let notifRefreshBalance: String = "refreshBalanceNotifier"
        static let notifCheckCache: String = "refreshCacheNotifier"
        static let notifCheckPush: String = "CheckPushInfo"
        static let notifShowChannels: String = "showChannelsNotification"
        static let notifForeground = "appInForeground"
        static let notifBackground = "appInBackground"
        static let notifVKAuth = "VKAuthed"
        static let notifShowVKShare = "showVKShare"
        static let notifCountMsg = "countMsgInDB"
        static let checkForNotifications = "checkForNotifications"
        static let notifStationRemoved = "stationRemovedNotification"
        static let notifFuelCompleted = "fuelCompletedNotification"
        static let notifFuelingStatus = "fuelingStatusNotification"
        static let notifFuelingError = "fuelingStatusError"
        static let notifAuthErr = "notificationAuthErrorLogout"
        static let notifAskUsrInfo = "askUserPersonalInfoNotification"
        static let notifShowFuelingRes = "notificationShowFuelingRes"
        static let kSection: String = "sectionKey"
        static let kMenu: String = "menu2insert"
        static let kHide: String = "hideMenu"
        static let kDinaMenus: String = "dynamicalMenus"
        static let kSelectedRow: String = "selectedRow"
        static let kAvatar: String = "avatar"
        static let kMenuTitle = "menuTitle"
        static let kNoSecrets = "benzo_use_secrets"
        static let kResults = "results"
        static let kAskRegInfo = "benzo_ask_registration_data"
        static let notifRefreshMenu = "benzo_notifRefreshMenu"
    }

    struct fstatus {
        static let kStat = "order_status"
        static let kCompleted = "Completed"
        static let kCanceled = "Canceled"
        static let kId = "order_id"
    }

    struct pages {
        static let offer = "oferta"
        static let urloffer = "/get_oferta"
        static let policy = "private_policy"
        static let urlpolicy = "/get_policy"
    }

    struct size {
        static let kImgMaxWidth: CGFloat = 100
    }

    struct pushLink {
        static let transactions: String = "balanceChanged"
        static let promos = "adAction"
        static let messages = "messages"
    }

    struct social {
        static let kVK = "vk"
        static let kOK = "ok"
        static let kFB = "fb"
        static let kGoogle = "g+"
        static let kErrGatherInfo = 127
        static let kErrUpdateToken = 128
    }

    struct touch {
        static let kFueling = "fueling"
        static let kAddMoney = "addMoney"
        static let kProfile = "profile"
        static let kTransactions = "transactions"
        static let kPromoCode = "promocode"
    }
}
