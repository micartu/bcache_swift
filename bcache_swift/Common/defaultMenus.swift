//
//  defaultMenus.swift
//  bcache_swift
//
//  Created by Michael Artuerhof on 19.06.19.
//  Copyright © 2019 Michael Artuerhof. All rights reserved.
//

import Foundation

func defaultMenus() -> [MenuSection] {
    return [sectionOf(entries: [menuOf(type: .map,
                                       title: "Gas stations".localized.uppercased()),
                                menuOf(type: .addMoney,
                                       title: "Add money".localized.uppercased()),
                                menuOf(type: .pay,
                                       title: "Pay".localized.uppercased())],
                      style: styleOf(fontSize: 20, weight: .medium, top: 30)),
            sectionOf(entries: [menuOf(type: .history,
                                       title: "Transactions".localized.uppercased()),
                                menuOf(type: .profile,
                                       title: "Profile".localized.uppercased()),
                                menuOf(type: .messages,
                                       title: "Messages".localized.uppercased()),
                                menuOf(type: .promo,
                                       title: "Promo".localized.uppercased()),
                                menuOf(type: .promoCode,
                                       title: "PromoCode".localized.uppercased()),
                                menuOf(type: .scanner,
                                       title: "Scanner".localized.uppercased()),
                                menuOf(type: .statistics,
                                       title: "Statistics".localized.uppercased())],
                      style: styleOf(fontSize: 15)),
            sectionOf(entries: [menuOf(type: .separator, title: "")], style: styleOf(fontSize: 0)),
            sectionOf(entries: [menuOf(type: .support,
                                       title: "Support Menu".localized),
                                menuOf(type: .offer,
                                       title: "An Offer".localized),
                                menuOf(type: .policy,
                                       title: "Privacy Policy".localized),
                                menuOf(type: .about,
                                       title: "About App".localized)],
                      style: styleOf(fontSize: 15))];
}
