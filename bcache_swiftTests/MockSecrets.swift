//
//  MockSecrets.swift
//  bcache_swiftTests
//
//  Created by Michael Artuerhof on 19.06.19.
//  Copyright © 2019 Michael Artuerhof. All rights reserved.
//

import Foundation
@testable import bcache_swift

class MockSecrets: SecretService {
    var authorized: Bool = false
    var isEncrypted: Bool = false
    var isAuthCreated: Bool = false
    var isSocialAuth: Bool = false
    var socialAuthType: String = ""
    var socialAuthToken: String = ""
    var socialAuthTokenId: String = ""
    var socialAuthExpire: Date? = nil
    var pwd: String = ""
    var magic: String = ""
    var session: String = ""
    var user: String = ""
    var password: String = ""
    var cardId: String = ""
    var uid: String = ""
    func canAccessKeychain() -> Bool {
        return false
    }

    func restoreKeyFromEnclave() -> Bool {
        return true
    }

    func saveKeyToEnclave() {
    }

    func erase() {
    }

    func encrypt(_ s: String) -> String {
        return s
    }

    func decrypt(_ s: String) -> String {
        return s
    }
}
