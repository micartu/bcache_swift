//
//  cacheTests.swift
//  benzoTests
//
//  Created by Michael Artuerhof on 29.05.19.
//  Copyright © 2019 MLN Group. All rights reserved.
//

import XCTest
@testable import bcache_swift

let kUsrPhone = "71234567890"

class cacheTests: XCTestCase {
    override func setUp() {
        super.setUp()
        cache = Cache(secrets: secrets, expirationInterval: 5)
        let _ = sys.application(UIApplication.shared, didFinishLaunchingWithOptions: [:])
    }

    override func tearDown() {
        super.tearDown()
    }

    func testUserCreation() {
        let exp = self.expectation(description: "user creation")
        createUser { [unowned self] in
            let exist = self.cache.exists(user: self.kPhone)
            XCTAssert(exist == true)
            let u = self.cache.get(user: self.kPhone)
            XCTAssert(u?.name == self.kName)
            XCTAssert(u?.loyality.cpa_crd_code == self.kPhone)
            self.cache.delete(user: self.kPhone) {
                XCTAssertFalse(self.cache.exists(user: self.kPhone))
                exp.fulfill()
            }
        }
        wait(for: [exp], timeout: kTimeout)
    }

    func testStationCreation() {
        let exp = self.expectation(description: "station creation")
        createStation { [unowned self] in
            if var s = self.cache.get(station: self.stationId) {
                XCTAssert(s.name == self.kStationName)
                XCTAssert(self.cache.exists(patch: s.patchId))
                let changedAddress = "shit"
                s.address = changedAddress
                self.cache.update(station: s) {
                    let sUpdated = self.cache.get(station: self.stationId)
                    XCTAssert(sUpdated?.address == changedAddress)
                    self.cache.delete(station: s) {
                        let sn = self.cache.get(station: self.stationId)
                        XCTAssert(sn == nil)
                        XCTAssertFalse(self.cache.exists(patch: s.patchId))
                        exp.fulfill()
                    }
                }
            }
        }
        wait(for: [exp], timeout: kTimeout)
    }

    func testTransactions() {
        let exp = self.expectation(description: "transactions tests")
        let kCount = 4
        createUser { [unowned self] in
            let tt = self.generateTransactions(count: kCount)
            self.cache.create(transactions: tt) {
                let tr = self.cache.getTransactions(from: nil, page: 0, count: kCount + 10)
                XCTAssert(tr.count == kCount, "count of transactions was: \(tr.count)")
                // check sorting order:
                var t = tr[0]
                XCTAssert(t.rrn == "0", "rrn was: \(t.rrn)")
                t = tr[1]
                XCTAssert(t.rrn == "1", "rrn was: \(t.rrn)")
                self.cache.delete(user: self.kPhone) {
                    exp.fulfill()
                }
            }
        }
        wait(for: [exp], timeout: kTimeout)
    }

    func testWebpage() {
        let exp = self.expectation(description: "webpage tests")
        createUser { [unowned self] in
            let id = "yahoo page"
            let p = WebPage(id: id, content: id)
            self.cache.create(webpage: p) {
                let w = self.cache.get(webpage: id)
                XCTAssert(w?.content == id)
                self.cache.delete(user: self.kPhone) {
                    let non = self.cache.get(webpage: id)
                    XCTAssert(non == nil)
                    exp.fulfill()
                }
            }
        }
        wait(for: [exp], timeout: kTimeout)
    }

    func testLocationCreation() {
        let exp = self.expectation(description: "location creation")
        createUser { [unowned self] in
            let g = DispatchGroup()
            let gen = [1, 2, 3]
            for i in gen {
                let l = LocPoint(lat: Double(i), lon: Double(i))
                g.enter()
                self.cache.add(locpoint: l) {
                    g.leave()
                }
            }
            g.notify(queue: .main) {
                let locs = self.cache.getLocPoints()
                XCTAssert(locs != nil)
                XCTAssert(locs?.count == gen.count)
                self.cache.delete(user: self.kPhone) {
                    let locs = self.cache.getLocPoints()
                    XCTAssert(locs == nil)
                    exp.fulfill()
                }
            }
        }
        wait(for: [exp], timeout: kTimeout)
    }

    func testMessagesLink() {
        let exp = self.expectation(description: "messages creation")
        createUser { [unowned self] in
            let a = self.generateMsg(link: "")
            let b = self.generateMsg(link: a.id)
            let c = self.generateMsg(link: b.id)
            let d = self.generateMsg(link: b.id)
            self.cache.create(messages: [a, b, c, d]) {
                let msgs = self.cache.getMessagesForCurrentUser(linked: a.id, category: "")
                XCTAssert(msgs?.count == 3, "but count of messages was: \(String(describing: msgs?.count))")
                self.cache.delete(user: self.kPhone) {
                    exp.fulfill()
                }
            }
        }
        wait(for: [exp], timeout: kTimeout)
    }

    func testMenuStoringCapabilities() {
        let exp = self.expectation(description: "menu sections creation/deletion")
        createUser { [unowned self] in
            let menus = defaultMenus()
            self.cache.storeForCurrentUser(menuSections: menus) {
                let mm = self.cache.getStoredMenuSectionsForCurrentUser()
                XCTAssertTrue(mm?.count == menus.count,
                              "but it was \(String(describing: mm?.count))")
                if let s0 = mm?.first{
                    XCTAssertTrue(s0.entries.count == menus[0].entries.count)
                    XCTAssertTrue(s0.entries[0].title == menus[0].entries[0].title)
                } else {
                    XCTAssertTrue(false)
                }
                self.cache.deleteAllMenuSectionsForCurrentUser {
                    let mm = self.cache.getStoredMenuSectionsForCurrentUser()
                    XCTAssertTrue(mm?.count == 0)
                    exp.fulfill()
                }
            }
        }
        wait(for: [exp], timeout: kTimeout)
    }

    private func generateMsg(link: String, category: String = "") -> Message {
        let id = UUID().uuidString
        return Message(id: id,
                       title: "title of \(id)",
                       body: "body of \(id)",
                       created: NSDate(),
                       infoLink: "",
                       link: link,
                       read: false,
                       subcategory: "",
                       tag: "",
                       button: "",
                       document: "",
                       category: category)
    }

    private func createUser(completion: @escaping (() -> Void)) {
        let l = UserLoyality(cpa_crd_code: kPhone,
                             cpa_crd_rub: 0,
                             cpa_prt_code: kPhone,
                             cpa_prt_name: "",
                             cpa_prt_ref1: kPhone,
                             cpa_prt_ref2: "",
                             cpa_prt_ref3: "",
                             cpa_prt_subid: kPhone)
        let u = User(login: kPhone,
                     phone: kPhone,
                     name: kName,
                     surname: "smith",
                     midname: "",
                     avatar: "avatar",
                     fuel: "ai92",
                     codeword: "buzz",
                     card: "XXX",
                     pin: "0|1234",
                     email: "mail@mail.com",
                     balance: 123,
                     loyality: l,
                     cardBlocked: true,
                     apn: "XYX")
        cache.create(user: u) { [weak self] in
            self?.secrets.user = self?.kPhone ?? ""
            completion()
        }
    }

    private func createStation(completion: @escaping (() -> Void)) {
        let lat: Double = 1
        let lon: Double = 2
        let s = Station(id: 0,
                        patchId: getPatchId(lon: lon, lat: lat),
                        name: kStationName,
                        descr: "descr",
                        address: "address",
                        inCarBuyFuel: false,
                        prices: "",
                        imgSmall: "logo",
                        imgBig: "logo_big",
                        lon: lon,
                        lat: lat,
                        countCollumn: 5)
        cache.create(station: s) { [weak self] id in
            self?.stationId = id
            completion()
        }
    }

    private func generateTransactions(count: Int) -> [Transaction] {
        var out = [Transaction]()
        for i in 0..<count {
            let t = Transaction(rrn: "\(i)",
                date: Date(timeIntervalSinceNow: -TimeInterval(i * 60)) as NSDate,
                trnType: "",
                brandName: "",
                location: "",
                productName: "",
                productQuantity: 1,
                trnBB: 0,
                trnValue: 0,
                points: 0,
                status: "",
                trnComment: "",
                convertBonusDate: NSDate(),
                discountSign: 0,
                discountValue: 1,
                discountValueCurrency: 0,
                channel: "",
                innertype: "")
            out.append(t)
        }
        return out
    }

    private var cache: Cache!
    private var stationId = 1
    private let kStationName = "GAZZ"
    private let secrets = MockSecrets()
    private let kPhone = "12345"
    private let kName = "john"
    private var sys = RealmInit()
}
