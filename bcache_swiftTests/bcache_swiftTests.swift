//
//  bcache_swiftTests.swift
//  bcache_swiftTests
//
//  Created by Michael Artuerhof on 19.06.19.
//  Copyright © 2019 Michael Artuerhof. All rights reserved.
//

import XCTest
import MagicalRecord
@testable import bcache_swift

let kTimeout: TimeInterval = 5
let kTestUser = "test_user_login"
let kTPersonPhoneId = "_personal_phone_id_"

class storageTests: XCTestCase {
    let secret = MockSecrets()
    var storage: StorageService!
    let kPhone = "12345"
    let kName = "john"
    let kStationName = "GAZZ"
    var stationId = 1
    var sys = CoreDataInit()

    override func setUp() {
        super.setUp()
        storage = Storage(secrets: secret, expirationInterval: 0)
        let _ = sys.application(UIApplication.shared, didFinishLaunchingWithOptions: [:])
    }

    override func tearDown() {
        super.tearDown()
    }

    func testUserCreation() {
        let exp = self.expectation(description: "user creation")
        createUser { [unowned self] in
            let exist = self.storage.exists(user: self.kPhone)
            XCTAssert(exist == true)
            let u = self.storage.get(user: self.kPhone)
            XCTAssert(u?.name == self.kName)
            self.storage.delete(user: self.kPhone) {
                exp.fulfill()
            }
        }
        wait(for: [exp], timeout: kTimeout)
    }

    func testUserUpdate() {
        let exp = self.expectation(description: "update user")
        createUser { [unowned self] in
            let kUpdatedName = "test"
            var u = self.storage.get(user: self.kPhone)!
            u.name = kUpdatedName
            self.storage.update(user: u) {
                let u2 = self.storage.get(user: self.kPhone)
                XCTAssert(u2?.name == kUpdatedName)
                self.storage.delete(user: self.kPhone) {
                    exp.fulfill()
                }
            }
        }
        wait(for: [exp], timeout: kTimeout)
    }

    func testStationCreation() {
        let exp = self.expectation(description: "station creation")
        createStation { [unowned self] in
            DispatchQueue.main.async {
                let s = (self.storage as! CacheStationService).get(station: self.stationId)
                XCTAssert(s != nil && s?.name == self.kStationName)
                XCTAssert(self.storage.exists(patch: s!.patchId))
                (self.storage as! CacheStationService).delete(station: s!) {
                    XCTAssertFalse(self.storage.exists(patch: s!.patchId))
                    exp.fulfill()
                }
            }
        }
        wait(for: [exp], timeout: kTimeout)
    }

    func testStationUpdate() {
        let exp = self.expectation(description: "station update")
        createStation { [unowned self] in
            let kUpdatedName = "test"
            var s = (self.storage as! CacheStationService).get(station: self.stationId)!
            s.name = kUpdatedName
            (self.storage as! CacheStationService).update(station: s) {
                let s1 = (self.storage as! CacheStationService).get(station: self.stationId)!
                XCTAssert(s1.name == s.name)
                (self.storage as! CacheStationService).delete(station: s) {
                    exp.fulfill()
                }
            }
        }
        wait(for: [exp], timeout: kTimeout)
    }

    func testTransactions() {
        let exp = self.expectation(description: "transactions tests")
        let kCount = 4
        createUser { [unowned self] in
            let tt = generateTransactions(count: kCount)
            let g = DispatchGroup()
            for t in tt {
                g.enter()
                self.storage.create(transaction: t) {
                    print("created in storage: \(t.rrn)")
                    g.leave()
                }
            }
            g.notify(queue: .main) {
                let tr = self.storage.getTransactions(from: nil, page: 0, count: kCount + 10)
                XCTAssert(tr.count == kCount, "count of transactions was: \(tr.count)")
                // check sorting order:
                var t = tr[0]
                XCTAssert(t.rrn == "0", "rrn was: \(t.rrn)")
                t = tr[1]
                XCTAssert(t.rrn == "1", "rrn was: \(t.rrn)")
                self.storage.delete(user: self.kPhone) {
                    exp.fulfill()
                }
            }
        }
        wait(for: [exp], timeout: kTimeout)
    }

    func testLocationCreation() {
        let exp = self.expectation(description: "location creation")
        createUser { [unowned self] in
            let g = DispatchGroup()
            let gen = [1, 2, 3]
            for i in gen {
                let l = LocPoint(lat: Double(i), lon: Double(i))
                g.enter()
                self.storage.add(locpoint: l) {
                    g.leave()
                }
            }
            g.notify(queue: .main) {
                let locs = self.storage.getLocPoints()
                XCTAssert(locs != nil)
                XCTAssert(locs?.count == gen.count)
                self.storage.delete(user: self.kPhone) {
                    exp.fulfill()
                }
            }
        }
        wait(for: [exp], timeout: kTimeout)
    }

    func testMessagesLink() {
        let exp = self.expectation(description: "messages creation")
        createUser { [unowned self] in
            let g = DispatchGroup()
            let a = self.generateMsg(link: "")
            let b = self.generateMsg(link: a.id)
            let c = self.generateMsg(link: b.id)
            let d = self.generateMsg(link: b.id)
            for m in [a, b, c, d] {
                g.enter()
                self.storage.create(message: m) {
                    g.leave()
                }
            }
            g.notify(queue: .main) {
                let msgs = self.storage.getMessagesForCurrentUser(linked: a.id, category: "")
                XCTAssert(msgs?.count == 3, "but count of messages was: \(String(describing: msgs?.count))")
                self.storage.delete(user: self.kPhone) {
                    exp.fulfill()
                }
            }
        }
        wait(for: [exp], timeout: kTimeout)
    }

    func testMenuStoringCapabilities() {
        let exp = self.expectation(description: "menu sections creation/deletion")
        createUser { [unowned self] in
            let menus = defaultMenus()
            self.storage.storeForCurrentUser(menuSections: menus) {
                let mm = self.storage.getStoredMenuSectionsForCurrentUser()
                XCTAssertTrue(mm?.count == menus.count,
                              "but it was \(String(describing: mm?.count))")
                if let s0 = mm?.first{
                    XCTAssertTrue(s0.entries.count == menus[0].entries.count)
                    XCTAssertTrue(s0.entries[0].title == menus[0].entries[0].title)
                } else {
                    XCTAssertTrue(false)
                }
                self.storage.deleteAllMenuSectionsForCurrentUser {
                    let mm = self.storage.getStoredMenuSectionsForCurrentUser()
                    XCTAssertTrue(mm?.count == 0)
                    exp.fulfill()
                }
            }
        }
        wait(for: [exp], timeout: kTimeout)
    }

    // MARK: - Private

    private func generateMsg(link: String, category: String = "") -> Message {
        let id = UUID().uuidString
        return Message(id: id,
                       title: "title of \(id)",
            body: "body of \(id)",
            created: NSDate(),
            infoLink: "",
            link: link,
            read: false,
            subcategory: "",
            tag: "",
            button: "",
            document: "",
            category: category)
    }

    private func createUser(completion: @escaping (() -> Void)) {
        let u = User(login: kPhone,
                     phone: kPhone,
                     name: kName,
                     surname: "smith",
                     midname: "",
                     avatar: "avatar",
                     fuel: "ai92",
                     codeword: "buzz",
                     card: "XXX",
                     pin: "0|1234",
                     email: "mail@mail.com",
                     balance: 123,
                     loyality: UserLoyality(),
                     cardBlocked: true,
                     apn: "XYX")
        storage.create(user: u) { [weak self] in
            self?.secret.user = self?.kPhone ?? ""
            completion()
        }
    }

    private func createStation(completion: @escaping (() -> Void)) {
        let lat: Double = 1
        let lon: Double = 2
        let s = Station(id: 0,
                        patchId: getPatchId(lon: lon, lat: lat),
                        name: kStationName,
                        descr: "descr",
                        address: "address",
                        inCarBuyFuel: false,
                        prices: "",
                        imgSmall: "logo",
                        imgBig: "logo_big",
                        lon: lon,
                        lat: lat,
                        countCollumn: 5)
        (self.storage as! CacheStationService).create(station: s) { [weak self] id in
            self?.stationId = id
            completion()
        }
    }
}

func generateTransactions(count: Int) -> [Transaction] {
    var out = [Transaction]()
    for i in 0..<count {
        let t = Transaction(rrn: "\(i)",
            date: Date(timeIntervalSinceNow: -TimeInterval(i * 60)) as NSDate,
            trnType: "",
            brandName: "",
            location: "",
            productName: "",
            productQuantity: 1,
            trnBB: 0,
            trnValue: 0,
            points: 0,
            status: "",
            trnComment: "",
            convertBonusDate: NSDate(),
            discountSign: 0,
            discountValue: 1,
            discountValueCurrency: 0,
            channel: "",
            innertype: "")
        out.append(t)
    }
    return out
}
